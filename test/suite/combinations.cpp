#include <catch.hpp>

#include <iostream>

#include <string>
#include <vector>

#include "core/combinations.h"

TEST_CASE("Combinations simple", "[Combinations]") {
  using core::Combinations;
  using std::string;
  using std::vector;

  vector<vector<string>> vectors = {{{"a", "b", "c"}, {"1", "2"}}};
  Combinations<decltype(vectors)> comb = vectors;

  int total = 0;

  for (const auto& combination : comb) ++total;

  CHECK(total == 6);

  auto first = comb.begin();
  CHECK(*first->at(0) == "a");
  CHECK(*first->at(1) == "1");
}

TEST_CASE("Combinations", "[Combinations]") {
  using core::Combinations;
  using std::string;
  using std::vector;

  vector<string> a = {"a", "b", "c"};
  vector<string> b = {"1", "2"};
  vector<string> c = {"$"};

  vector<string> result = {"a1$", "a2$", "b1$", "b2$", "c1$", "c2$"};

  vector<vector<string>> lists{a, b, c};
  Combinations<decltype(lists)> comb(lists);
  std::size_t total = 0;

  auto rit = result.cbegin();

  for (const auto& combination : comb) {
    std::stringstream ss;
    for (const auto& it : combination) ss << *it;
    CHECK(ss.str() == *rit);
    if (rit != result.end()) ++rit;
    ++total;
  }
  CHECK(total == result.size());
}
