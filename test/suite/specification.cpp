#include <catch.hpp>

#include <cstdlib>

#include <array>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

#include "core/filesystem.h"
#include "eds/semisupervised/specification.h"

const std::string kFile = "../test/suite/specification.json";

TEST_CASE("SSL Specification", "[Specification]") {
  using eds::ssl::Specification;

  REQUIRE(filesystem::exists(kFile));
  std::ifstream file(kFile);
  Specification spec(file);

  CHECK(spec.problem() == "semisupervised learning");
  CHECK(spec.algorithm() == "edge dynamics system");
  CHECK(spec.get("input.prefix") == "/tmp/torus-input/");
  CHECK(spec.get<std::string>("input.network.type") == "pajek");
  CHECK(spec.get<std::string>("output.type") == "terminal");
  CHECK(spec.neighborhood_orders().size() == 3);

  CHECK(spec.inputs().size() == 5544);
  CHECK(spec.inputs().at(1).alg.lambda == Approx(0.25));
}

class TestSpec : public core::Specification {
 public:
  TestSpec(std::istream& json) { read(json); }
  virtual ~TestSpec() {}

  std::vector<int> alpha_list;
  std::vector<std::vector<int>> lists;
  std::vector<std::vector<int>> combinations;
  std::vector<std::string> keys;

 private:
  virtual void ProcessTree() {
    auto to_int = [](const auto& v) -> int { return std::atoi(v.c_str()); };

    alpha_list = ReadList<int>(Node("alpha list"), to_int);

    lists = ReadLists<int>(Node("second level"), core::spec::default_use_key,
                           to_int);

    GetCombinations<std::string, int>(
        Node("second level"), [&](const auto&, const auto& iterators) {
          combinations.emplace_back();
          auto& cb = combinations.back();
          cb.reserve(iterators.size());
          for (const auto& it : iterators) {
            cb.push_back(*it);
          }
        }, core::spec::default_use_key, to_int);

    keys = ReadKeys(Node("second level"));
  }
};

TEST_CASE("Reference lists", "[Specification]") {
  std::istringstream json(R"(
{
  "second level": {
    "a": [1, 2],
    "b": "first level.c"
  },
  "first level" : {
    "c": "alpha list"
  },
  "alpha list": [2, 1, 3]
}
)");

  TestSpec spec(json);

  REQUIRE(spec.keys.size() == 2);
  CHECK(spec.keys[0] == "a");
  CHECK(spec.keys[1] == "b");  // Actually is 'alpha list' list

  /* "alpha list" */
  REQUIRE(spec.alpha_list.size() == 3);
  CHECK(spec.alpha_list[0] == 2);
  CHECK(spec.alpha_list[1] == 1);
  CHECK(spec.alpha_list[2] == 3);

  /* "second level" lists */
  REQUIRE(spec.lists.size() == 2);

  REQUIRE(spec.lists[0].size() == 2);
  CHECK(spec.lists[0].at(0) == 1);
  CHECK(spec.lists[0].at(1) == 2);

  REQUIRE(spec.lists[1].size() == 3);
  CHECK(spec.lists[1].at(0) == 2);
  CHECK(spec.lists[1].at(1) == 1);
  CHECK(spec.lists[1].at(2) == 3);

  /* "second level" combinations */
  REQUIRE(spec.combinations.size() == 6);

#define CHECK_COMBINATION(idx, first, second)   \
  CHECK(spec.combinations[idx].at(0) == first); \
  CHECK(spec.combinations[idx].at(1) == second);

  CHECK_COMBINATION(0, 1, 2);
  CHECK_COMBINATION(1, 1, 1);
  CHECK_COMBINATION(2, 1, 3);
  CHECK_COMBINATION(3, 2, 2);
  CHECK_COMBINATION(4, 2, 1);
  CHECK_COMBINATION(5, 2, 3);

#undef CHECK_COMBINATION
}
