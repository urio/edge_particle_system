#include <catch.hpp>

#include <array>
#include <vector>

#include "core/file_information.h"

std::array<core::FileInformation, 4> prepare() {
  using core::FileInformation;

  FileInformation a;
  a.name = "file a";
  a.param["alpha"] = 1.0;
  a.param["beta"] = 1.0;
  a.param["zeta"] = 6.4;
  a.param["iota"] = 9.0;

  FileInformation b;
  b.name = "file b";
  b.param["alpha"] = 1.0;
  b.param["beta"] = 2.0;
  b.param["zeta"] = 0.4;
  b.param["iota"] = 3.0;

  FileInformation c;
  c.name = "file c";
  c.param["alpha"] = 1.0;
  c.param["zeta"] = 6.4;
  c.param["iota"] = 3.0;

  FileInformation d;
  d.name = "file d";

  return {{a, b, c, d}};
}

TEST_CASE("Empty", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  /* Match empty lists */
  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 0);

  /* Match without criteria */
  std::vector<FileInformation> l1{files[0]};
  std::vector<FileInformation> l2{files[1]};
  matcher.push(l1);
  matcher.push(l2);
  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 2);
  CHECK(result[0].name == "file a");
  CHECK(result[1].name == "file b");
}

TEST_CASE("Missing keys", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  matcher.add_criterion("gamma");

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[2]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 4);
  CHECK(result[0].name == "file a");
  CHECK(result[1].name == "file c");
  CHECK(result[2].name == "file b");
  CHECK(result[3].name == "file c");
}

TEST_CASE("Mixed missing keys", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  matcher.add_criterion("gamma");
  matcher.add_criterion("alpha");

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[2]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 4);
  CHECK(result[0].name == "file a");
  CHECK(result[1].name == "file c");
  CHECK(result[2].name == "file b");
  CHECK(result[3].name == "file c");
}

TEST_CASE("Missing key on second list", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  matcher.add_criterion("alpha");
  matcher.add_criterion("zeta");

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[2]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 2);
  CHECK(result[0].name == "file a");
  CHECK(result[1].name == "file c");
}

TEST_CASE("Impossible match", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  matcher.add_criterion("beta");
  matcher.add_criterion("zeta");
  matcher.add_criterion("iota");

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[2]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 0);
}

TEST_CASE("Mixed case 3", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  matcher.add_criterion("beta");
  matcher.add_criterion("iota");

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[2]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 2);
  CHECK(result[0].name == "file b");
  CHECK(result[1].name == "file c");
}

TEST_CASE("Empty params", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  std::vector<FileInformation> l1{files[3]};
  matcher.push(l1);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
  });
  REQUIRE(result.size() == 1);
  CHECK(result[0].name == "file d");
}

TEST_CASE("Empty params 2", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  std::vector<FileInformation> l1{files[3]};
  matcher.push(l1);
  matcher.push(l1);

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 2);
  CHECK(result[0].name == "file d");
  CHECK(result[1].name == "file d");
}

TEST_CASE("Empty params 3", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  std::vector<FileInformation> l1{files[3], files[3]};
  std::vector<FileInformation> l2{files[3]};
  matcher.push(l1);
  matcher.push(l2);

  matcher.add_criterion("kilo");

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
  });
  REQUIRE(result.size() == 4);
  CHECK(result[0].name == "file d");
  CHECK(result[1].name == "file d");
  CHECK(result[2].name == "file d");
  CHECK(result[3].name == "file d");
}

TEST_CASE("Param and empty param maps", "[FileInformationMatcher]") {
  using core::FileInformation;
  using core::FileInformationMatcher;

  const auto& files = prepare();
  FileInformationMatcher matcher;
  std::vector<FileInformation> result;

  std::vector<FileInformation> l1{files[0], files[1]};
  std::vector<FileInformation> l2{files[3]};
  std::vector<FileInformation> l3{files[2]};
  matcher.push(l1);
  matcher.push(l2);
  matcher.push(l3);

  matcher.add_criterion("iota");
  matcher.add_criterion("alpha");

  matcher.match([&](const auto& iterators) {
    result.push_back(*iterators[0]);
    result.push_back(*iterators[1]);
    result.push_back(*iterators[2]);
  });
  REQUIRE(result.size() == 3);
  CHECK(result[0].name == "file b");
  CHECK(result[1].name == "file d");
  CHECK(result[2].name == "file c");
}
