#include <armadillo>

#include "core/utility.h"

#undef INFO
#undef WARN
#include <catch.hpp>

TEST_CASE("max_col", "[Utility]") {
  using arma::TieMethod;

  arma::mat X = {{0, 1, 2, 3, 4, 5},
                 {5, 4, 3, 2, 1, 0},
                 {0, 1, 0, 1, 0, 1},
                 {1, 1, 1, 1, 1, 1}};
  REQUIRE(X.n_rows == 4);
  REQUIRE(X.n_cols == 6);
  auto rnd = arma::max_col(X, TieMethod::Random);
  CHECK(rnd(0) == 5);
  CHECK(rnd(1) == 0);
  if (!((rnd(2) == 1) || (rnd(2) == 3) || (rnd(2) == 5))) CHECK(false);

  auto first = arma::max_col(X, TieMethod::First);
  CHECK(first(0) == 5);
  CHECK(first(1) == 0);
  CHECK(first(2) == 1);
  CHECK(first(3) == 0);

  auto last = arma::max_col(X, TieMethod::Last);
  CHECK(last(0) == 5);
  CHECK(last(1) == 0);
  CHECK(last(2) == 5);
  CHECK(last(3) == 5);

  arma::mat empty;
  auto ecol = arma::max_col(empty);
  CHECK(ecol.size() == 0);
}


TEST_CASE("max_row", "[Utility]") {
  using arma::TieMethod;

  arma::rowvec X = {{0, 1, 0, 1, 0, 1}};
  REQUIRE(X.size() == 6);
  auto rnd = arma::max_rowvec(X, TieMethod::Random);
  if (!((rnd == 1) || (rnd == 3) || (rnd == 5))) CHECK(false);

  auto first = arma::max_rowvec(X, TieMethod::First);
  CHECK(first == 1);
  
  auto last = arma::max_rowvec(X, TieMethod::Last);
  CHECK(last == 5);
}

