#include <string>

#include <jules/dataframe/column.hpp>

#include "core/categorical.h"
#include "eds/semisupervised/sources_lists.h"

#undef INFO
#undef WARN
#include <catch.hpp>

TEST_CASE("String categories", "[Categorical]") {
  using jules::column;

  auto col = column{"1.0", "-1.0", "0.0", "-1.0", "1.0"};
  core::Categorical<std::string> categories(col);

  CHECK(categories.enumerate("-1.0") == 0);
  CHECK(categories.enumerate("0.0") == 1);
  CHECK(categories.enumerate("1.0") == 2);
}

TEST_CASE("Double categories", "[Categorical]") {
  using jules::column;

  auto col = column{"1.0", "-1.0", "0.0", "-1.0", "1.0"};
  auto dcol = jules::coerce_to<double>(col);
  core::Categorical<double> categories(dcol);

  CHECK(categories.enumerate(-1.0) == 0);
  CHECK(categories.enumerate(0.0) == 1);
  CHECK(categories.enumerate(1.0) == 2);
}

TEST_CASE("String named categories", "[Categorical]") {
  using jules::column;

  auto col = column{"Fish", "Amoeba", "Aligator", "Duck", "Tiger"};
  core::Categorical<std::string> categories(col);

  CHECK(categories.enumerate("Aligator") == 0);
  CHECK(categories.enumerate("Amoeba") == 1);
  CHECK(categories.enumerate("Duck") == 2);
  CHECK(categories.enumerate("Fish") == 3);
  CHECK(categories.enumerate("Tiger") == 4);
}

TEST_CASE("Numbered categories", "[Categorical]") {
  using jules::column;

  auto col = column{1, 2, 10, 4, 3, 6, 7, 5, 12, 11, 9, 8};
  core::Categorical<double> categories(col);

  for (int i = 0; i < 12; ++i)
    CHECK(categories.enumerate(static_cast<double>(i + 1)) == i);
}

TEST_CASE("Numbered categories x", "[Categorical]") {
  using jules::column;

  auto col = column{1, 2, 10, 4, 3, 6, 7, 5, 12, 11, 9, 8};
  core::Categorical<double> categories(col);

  for (int i = 0; i < 12; ++i)
    CHECK(categories.enumerate(static_cast<double>(i + 1)) == i);
}

TEST_CASE("12 String numbered categories", "[Categorical]") {
  using jules::column;

  auto col =
      column{"1", "2", "10", "4", "3", "6", "7", "5", "12", "11", "9", "8"};
  std::array<int, 12> order = {{0, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3}};
  core::Categorical<std::string> categories(col);

  for (int i = 0; i < 12; ++i) {
    CHECK(categories.enumerate(std::to_string(i + 1)) == order[i]);
  }
}

TEST_CASE("10 String numbered categories", "[Categorical]") {
  using jules::column;

  auto col = column{"1", "2", "10", "4", "3", "6", "7", "5", "9", "8"};
  std::array<int, 10> order = {{0, 2, 3, 4, 5, 6, 7, 8, 9, 1}};
  core::Categorical<std::string> categories(col);

  for (int i = 0; i < 10; ++i) {
    CHECK(categories.enumerate(std::to_string(i + 1)) == order[i]);
  }
}

TEST_CASE("EDS SSL SourcesLists", "[SourcesLists]") {
  using jules::column;
  using eds::ssl::SourcesLists;

  auto col =
      column{"1", "2", "10", "4", "3", "6", "7", "5", "12", "11", "9", "8"};
  std::array<double, 5> pred = {{2, 5, 6, 4, 0}};
  SourcesLists sl{{col}, pred};

  CHECK(sl.is_source(0, 0));
  CHECK(sl.is_source(2, 4));

  CHECK(sl.is_source(9, 2));
  CHECK(sl.is_rival(8, 5));
  CHECK(sl.is_source(5, 5));
  CHECK(sl.is_source(6, 6));
}
