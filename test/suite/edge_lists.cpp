#include <catch.hpp>

#include <iostream>

#include <cstddef>

#include <igraphpp/igraph.hpp>

#include "core/edge_lists.h"

TEST_CASE("Edge Lists", "[Edge Lists]") {
  using igraph::Graph;
  using igraph::EdgeLists;

  Graph g = Graph::ErdosRenyi(100, 200);
  EdgeLists edges(g);

  CHECK(edges.size() == static_cast<std::size_t>(g.vcount()));

  int total = 0;
  for (const auto& lst : edges) {
    total += static_cast<int>(lst.size());
  }
  CHECK(total == 2 * g.ecount());
}
