
#include <catch.hpp>

#include <cstddef>

#include <igraphpp/igraph.hpp>

#include "core/edge_vector.h"

TEST_CASE("Edge Vector", "[Edge Vector]") {
  using igraph::Graph;
  using igraph::EdgeVector;

  Graph g = Graph::ErdosRenyi(100, 200);
  EdgeVector edges(g);

  CHECK(edges.size() == static_cast<std::size_t>(g.ecount()));
}
