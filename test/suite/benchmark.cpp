
#include <catch.hpp>

#include "core/benchmark.h"

TEST_CASE("Benchmark", "[Benchmark]") {
  using core::Benchmark;
  Benchmark b;
  REQUIRE_THROWS(b.cpu());
  REQUIRE_THROWS(b.real());
  b.push();
  REQUIRE_THROWS(b.cpu());
  REQUIRE_THROWS(b.real());
  b.push();
  REQUIRE(b.cpu(0) != 0.0);
  REQUIRE(b.real(0) != 0.0);
  REQUIRE_NOTHROW(b.cpu());
  REQUIRE_NOTHROW(b.real());
}
