#include <catch.hpp>

#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>

#include "core/output/terminal.h"
#include "core/output/file.h"
#include "core/filesystem.h"

#ifdef WITH_REDIS
#include "core/output/redis.h"
#endif

TEST_CASE("Simple terminal output", "[OutputPolicy]") {
  std::stringstream result_stream;
  core::TerminalOutput terminal{result_stream};

  std::unique_ptr<std::stringstream> ss(new std::stringstream);
  *ss.get() << "col1\tcol2\n";
  terminal.push({"test.tab", std::move(ss)});

  CHECK(result_stream.str() == "Identifier: test.tab\ncol1\tcol2\n");
}

static void thread_job(core::OutputPolicy& output) {
  std::unique_ptr<std::stringstream> ss(new std::stringstream);
  *ss.get() << "1\t2\n";
  output.push({"test.tab", std::move(ss)});
}

TEST_CASE("Threaded terminal output", "[OutputPolicy]") {
  std::stringstream result_stream;
  core::TerminalOutput terminal{result_stream};

  std::thread t1(thread_job, std::ref(terminal));
  std::thread t2(thread_job, std::ref(terminal));

  t1.join();
  t2.join();

  CHECK(result_stream.str() ==
        "Identifier: test.tab\n1\t2\nIdentifier: test.tab\n1\t2\n");
}

TEST_CASE("File output", "[OutputPolicy]") {
  std::stringstream result_stream;
  boost::property_tree::ptree p;
  core::FileOutput output(p);

  const std::string kFile = "___test.tab";

  if (filesystem::exists(kFile)) filesystem::remove(kFile);

  std::unique_ptr<std::stringstream> ss(new std::stringstream);
  *ss.get() << "col1\tcol2\n";
  output.push({kFile, std::move(ss)});

  CHECK(filesystem::exists(kFile));

  std::ifstream file(kFile);
  CHECK(file.is_open());
  if (file.is_open()) {
    std::stringstream buffer;
    buffer << file.rdbuf();
    CHECK(buffer.str() == "col1\tcol2\n");
  }

  filesystem::remove(kFile);
}

#ifdef WITH_REDIS
TEST_CASE("Redis output", "[OutputPolicy]") {
  core::RedisOutput redis;
  std::unique_ptr<std::stringstream> ss(new std::stringstream);
  *ss.get() << "col1\tcol2\n";
  redis.push({"key1", std::move(ss)});

  CHECK(redis.contains("key1"));
  CHECK_FALSE(redis.contains("key2"));
}
#endif  // WITH_REDIS
