
#include <string>

#include <jules/dataframe/column.hpp>
#include <igraphpp/igraph.hpp>

#include "core/benchmark.h"
#include "core/categorical.h"
#include "core/debug.h"
#include "core/utility.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/printer.h"
#include "eds/semisupervised/problem_input.h"

const std::string kPath = "../test/data/SSL6/";

int main(void) {
  using jules::column;
  using igraph::Graph;
  using jules::read_table;

  eds::ssl::ProblemInput input;
  input.alpha = 0.0;
  input.lambda = 0.5;

  core::Benchmark bench;

  bench.push();
  column Y = read_table<double>(kPath + "Y.tab").select(0);
  column labeled = read_table<double>(kPath + "labeled.tab").select(0);
  core::Categorical<std::string> predictive(Y);

  Graph network = Graph::ReadPajek(kPath + "network.net").to_undirected();
  igraph::EdgeLists edge_lists(network);
  igraph::EdgeVector edge_vector(network);
  eds::ssl::SourcesLists sources({Y}, labeled);

  input.network = &network;
  input.edge_vector = &edge_vector;
  input.edge_lists = &edge_lists;
  input.sources = &sources;

  for (auto index : labeled.view<double>()) {
    std::cout << index << " → " << predictive[index] << std::endl;
  }
  bench.push();
  printd(INFO, "Reading time:" BLDYLW " %s\n", bench.str().c_str());

  bench.push();
  eds::ssl::cpu::EdgeDynamicsSystem eds;
  eds::ssl::Printer printer(&eds);

  eds.Initialize(&input);
  bench.push();
  printd(INFO, "Initialization time:" BLDYLW " %s\n", bench.str().c_str());
  fflush(stdout);

  int epoch = 500;

  bench.push();
  while (epoch-- > 0) {
    eds.Epoch();
    // eds.notify();
  }
  // eds.notify();
  bench.push();
  printd(INFO, "Running time:" BLDYLW " %s\n", bench.str().c_str());
  return 0;
}
