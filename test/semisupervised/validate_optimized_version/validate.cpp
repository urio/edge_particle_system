#include <cstddef>

#include <string>

#include <jules/dataframe/column.hpp>
#include <igraphpp/igraph.hpp>

#include "core/debug.h"
#include "core/utility.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/cpu_eds_vanilla.h"
#include "eds/semisupervised/problem_input.h"

const std::string kPath = "../test/data/SSL6/";

eds::ssl::cpu::EdgeDynamicsSystemVanilla vanilla;
eds::ssl::cpu::EdgeDynamicsSystem optimized;

size_t errors = 0;

void check_diff(const char* name, const arma::mat& diff) {
  for (size_t i = 0; i < diff.n_rows; ++i) {
    for (size_t j = 0; j < diff.n_cols; ++j) {
      double d = diff(i, j);
      if (d > 1e-10) {
        printd(WARN, "%s - (%zu, %zu) diff = %.10lf\n", name, i, j, d);
        errors++;
      }
    }
  }
}

void check_internal_state() {
  using std::size_t;
  const size_t K = optimized.dynamics().classes;

  for (size_t k = 0; k < K; ++k) {
    const auto& eds_dom = optimized.dynamics().data[k].total_dominance;
    const auto& van_dom = vanilla.dynamics().data[k].total_dominance;
    check_diff("Total dominance", van_dom - eds_dom);
    const auto& eds_ncur = optimized.dynamics().data[k].current_dominance;
    const auto& van_ncur = vanilla.dynamics().data[k].current_dominance;
    check_diff("Current dominance", van_ncur - eds_ncur);
    const auto& eds_no = optimized.dynamics().data[k].no_particles;
    const auto& van_no = vanilla.dynamics().data[k].no_particles;
    check_diff("Number of particles", van_no - eds_no);
  }
}

int main(void) {
  using jules::column;
  using igraph::Graph;
  using jules::read_table;

  column Y = read_table<double>(kPath + "Y.tab").select(0);
  column labeled = read_table<double>(kPath + "labeled.tab").select(0);

  eds::ssl::ProblemInput input;
  input.alpha = 0.25;
  input.lambda = 0.5;
  input.max_epochs = 10;
  Graph network = Graph::ReadPajek(kPath + "network.net").to_undirected();
  igraph::EdgeLists edge_lists(network);
  igraph::EdgeVector edge_vector(network);
  eds::ssl::SourcesLists sources({Y}, labeled);

  input.network = &network;
  input.edge_vector = &edge_vector;
  input.edge_lists = &edge_lists;
  input.sources = &sources;

  optimized.Initialize(&input);
  vanilla.Initialize(&input);

  printf("Running optimized.\n");
  while (!optimized.IsFinished()) optimized.Epoch();

  printf("Running vanilla.\n");
  while (!vanilla.IsFinished()) vanilla.Epoch();

  printf("Edge dominance (old “dynamics”)\n");
  optimized.EdgeDominance().raw_print();

  printf("Validating. (Errors will be printed.)\n");
  check_internal_state();
  printf("Finished. Errors: %zd\n", errors);

  optimized.Finalize();
  vanilla.Finalize();

  return 0;
}
