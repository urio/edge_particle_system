
#include <unistd.h>

#include <iterator>
#include <set>
#include <string>

#include <jules/dataframe/dataframe.hpp>

#include "core/benchmark.h"
#include "core/debug.h"
#include "core/utility.h"
#include "core/visualization.h"
#include "eds/semisupervised/cpu_eds.h"

const std::string kPath = "../test/data/banana/";

int main(void) {
  using jules::dataframe;
  using jules::column;
  using igraph::Graph;
  using jules::read_table;

  core::Benchmark bench;

  bench.push();

  dataframe X = read_table(kPath + "X.tab");
  column Y = read_table<double>(kPath + "Y.tab").select(0);
  column labeled = read_table<double>(kPath + "labeled.tab").select(0);
  core::Categorical<std::string> predictive(Y);

  Graph network = Graph::ReadPajek(kPath + "network.net").to_undirected();
  igraph::EdgeLists edge_lists(network);
  igraph::EdgeVector edge_vector(network);
  eds::ssl::SourcesLists sources({Y}, labeled);

  eds::ssl::ProblemInput input;
  input.alpha = 0.0;
  input.lambda = 0.5;
  input.edge_lists = &edge_lists;
  input.edge_vector = &edge_vector;
  input.network = &network;
  input.sources = &sources;

  for (auto index : labeled.view<double>()) {
    std::cout << index << " → " << predictive[index] << std::endl;
  }
  bench.push();
  printd(INFO, "Reading time:" BLDYLW " %s\n", bench.str().c_str());

  bench.push();
  eds::ssl::cpu::EdgeDynamicsSystem eds;
  eds.Initialize(&input);
  bench.push();
  printd(INFO, "Initialization time:" BLDYLW " %s\n", bench.str().c_str());
  fflush(stdout);

  core::Visualization view;
  view.SetGraph(*input.network);
  view.SetCoordinates(X);
  view.SetVertexColors(Y);
  view.Initialize();

  int epoch = 100;

  bench.push();
  while (epoch-- > 0) {
    eds.Epoch();
    view.SetEdgeColors(eds.Result());
    view.Render();
    usleep(1000);
  }
  bench.push();
  printd(INFO, "Running time:" BLDYLW " %s\n", bench.str().c_str());

  view.Start();

  return 0;
}
