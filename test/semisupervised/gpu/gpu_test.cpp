
#include <string>

#include <jules/dataframe/column.hpp>
#include <igraphpp/igraph.hpp>

#include <benchmark.h>
#include <categorical.h>
#include <debug.h>
#include <problem_input.h>
#include <utility.h>
#include <semisupervised/gpu_eds.h>
#include <semisupervised/printer.h>

const std::string kPath = "../test/data/SSL6/";

int main(void) {
  using jules::column;
  using igraph::Graph;
  using jules::read_table;

  core::ProblemInput input;
  input.alpha = 0.0;
  input.lambda = 0.5;

  core::Benchmark bench;

  bench.push();
  column Y = read_table(kPath + "Y.tab").select(0);
  column labeled = read_table<double>(kPath + "labeled.tab").select(0);
  core::Categorical<std::string> predictive(Y);

  input.network = Graph::ReadPajek(kPath + "network.net").to_undirected();
  input.sources.Initialize(predictive, labeled);

  for (auto index : labeled.view<double>()) {
    std::cout << index << " → " << predictive[index] << std::endl;
  }
  bench.push();
  printd(INFO, "Reading time:" BLDYLW " %s\n", bench.str().c_str());

  bench.push();
  ssl::gpu::EdgeDynamicsSystem eds;
  ssl::Printer printer(&eds);

  eds.Initialize(&input);
  bench.push();
  printd(INFO, "Initialization time:" BLDYLW " %s\n", bench.str().c_str());
  fflush(stdout);

  int epoch = 100;  // 200;

  bench.push();
  while (epoch-- > 0) {
    eds.Epoch();
    // eds.notify();
  }
  eds.notify();
  bench.push();
  printd(INFO, "Running time:" BLDYLW " %s\n", bench.str().c_str());
  return 0;
}
