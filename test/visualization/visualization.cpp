
#include <cstddef>
#include <unistd.h>

#include <igraphpp/igraph.hpp>

#include "core/visualization.h"
#include "core/utility.h"

static jules::column make_edge_colors(int ecount, int k = 0) {
  auto ecol = jules::column{0.0, static_cast<std::size_t>(ecount)};
  auto vecol = ecol.view<double>();
  for (int i = 0; i < ecount; ++i) vecol[i] = (i + k) % 4;
  return ecol;
}

int main(void) {
  using core::Visualization;
  using jules::dataframe;
  using jules::column;
  using igraph::Graph;

  dataframe X = jules::read_table("../test/data/3d/X.tab");
  column Y = jules::read_table("../test/data/3d/Y.tab").select(0);
  Graph graph = Graph::ReadPajek("../test/data/3d/network.net").to_undirected();
  column edge_colors = make_edge_colors(graph.ecount());

  Visualization view;
  view.SetGraph(graph);
  view.SetCoordinates(X);
  view.SetVertexColors(Y);
  view.SetEdgeColors(edge_colors);
  view.SetEdgeLabels(edge_colors);
  view.Initialize();

  int k = 0;
  while (k < 100) {
    view.SetEdgeColors(make_edge_colors(graph.ecount(), k++));
    view.Render();
    usleep(1000);
  }

  view.Start();
  return 0;
}
