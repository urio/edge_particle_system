#include <string>

#include "core/utility.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/printer.h"
#include "eds/semisupervised/problem_input.h"

const std::string kPath = "../test/data/small/";

int main(void) {
  using jules::column;
  using igraph::Graph;

  /* Read input files */
  auto Y = jules::read_table<double>(kPath + "Y.tab").select(0);
  auto labeled = jules::read_table<double>(kPath + "labeled.tab").select(0);
  auto network = Graph::ReadPajek(kPath + "network.net").to_undirected();
  igraph::EdgeLists edge_lists(network);
  igraph::EdgeVector edge_vector(network);
  eds::ssl::SourcesLists sources({Y}, labeled);

  /* Prepare algorithm input */
  eds::ssl::ProblemInput input;
  input.alpha = 0.0;
  input.lambda = 0.5;
  input.max_epochs = 2;
  input.sources = &sources;
  input.network = &network;
  input.edge_vector = &edge_vector;
  input.edge_lists = &edge_lists;

  /* Run */
  eds::ssl::cpu::EdgeDynamicsSystem eds;
  eds::ssl::Printer printer(&eds);

  eds.Initialize(&input);
  eds.notify();  // Notify all observers. In this example, calls printer.
  while (!eds.IsFinished()) {
    eds.Epoch();
    eds.notify();
  }
  eds.Finalize();
  return 0;
}
