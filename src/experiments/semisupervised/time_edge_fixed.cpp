// vim: sw=2 ts=2

#include <string>
#include <chrono>
#include <random>

#include "core/utility.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/problem_input.h"

int main(void) {
  using jules::column;
  using jules::make_view;
  using igraph::Graph;
  using std::chrono::system_clock;
  using std::chrono::duration;

  std::bernoulli_distribution d;
  std::random_device rd;

  eds::ssl::cpu::EdgeDynamicsSystem eds;
  constexpr int E = 400000;

  std::cout << "V p E t" << std::endl;
  for (int rep = 0; rep < 10; ++rep) {
    for (std::size_t V = 1000; V <= 2000; V += 100) {
      jules::column Y(0.0, V);
      auto Y_view = make_view<double>(Y);
      std::generate(Y_view.begin(), Y_view.end(), [&]() { return d(rd) ? 1.0 : 0.0; });

      jules::column labeled(0.0, static_cast<std::size_t>(std::round(V * 0.05)));
      auto labeled_view = make_view<double>(labeled);
      std::iota(labeled_view.begin(), labeled_view.end(), 0.0);

      auto network = Graph::ErdosRenyi(V, E, false);

      igraph::EdgeLists edge_lists(network);
      igraph::EdgeVector edge_vector(network);
      eds::ssl::SourcesLists sources({Y}, labeled);

      /* Prepare algorithm input */
      eds::ssl::ProblemInput input;
      input.alpha = 0.0;
      input.lambda = 1.0;
      input.max_epochs = 30;
      input.sources = &sources;
      input.network = &network;
      input.edge_vector = &edge_vector;
      input.edge_lists = &edge_lists;

      /* Run */
      eds.Initialize(&input);
      while (!eds.IsFinished()) {
        auto start = system_clock::now();
        eds.Epoch();
        auto end = system_clock::now();
        std::cout << V << " " << (static_cast<double>(E) / (V*V))
                  << " " << E << " "
                  << duration<double>{end - start}.count() << std::endl;
      }
      eds.Finalize();
    }
  }

  return 0;
}
