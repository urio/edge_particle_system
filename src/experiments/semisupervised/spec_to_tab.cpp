/* This program reads and JSON specification and converts it to tabular
 * format.  It may be useful to process the output in another environment that
 * does not process this JSON specification. */
#include <cstdlib>

#include <fstream>

#include "eds/semisupervised/specification.h"

static void help(int, char* argv[]) {
  fprintf(stderr, "Format: %s specification.json\n", argv[0]);
  exit(1);
}

int main(int argc, char* argv[]) {
  if (argc != 2) help(argc, argv);

  std::ifstream json{argv[1]};
  eds::ssl::Specification spec(json);
  const auto& inputs = spec.inputs();

  if (inputs.empty()) return 1;

  inputs.begin()->header(std::cout);
  std::cout << "\n";

  for (const auto& input : spec.inputs()) {
    input.row(std::cout);
    std::cout << "\n";
  }

  return 0;
}
