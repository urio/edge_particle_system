#include <mutex>
#include <thread>

#include "core/utility.h"
#include "core/filesystem.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/printer.h"
#include "eds/semisupervised/problem_input.h"

struct Run {
  std::string input_path, output_path;

  // Network
  unsigned nsamples, p, q;
  float sd;
  unsigned k;

  // Y
  unsigned nclasses;

  // Labeled
  unsigned rep;
  float plabeled;

  // EDS
  double alpha, lambda;
  int max_epochs;

  unsigned input_precision;
};

struct NetworkRun {
  NetworkRun(const std::string& line) {
    std::stringstream ss{line};
    ss >> nsamples >> p >> q >> sd >> k;
  }
  NetworkRun(const Run& run)
      : nsamples{run.nsamples}, p{run.p}, q{run.q}, sd{run.sd}, k{run.k} {}

  bool operator==(const NetworkRun& other) const {
    return nsamples == other.nsamples && p == other.p && q == other.q &&
           sd == other.sd && k == other.k;
  }
  unsigned nsamples, p, q;
  float sd;
  unsigned k;
};

struct YRun {
  YRun(const std::string& line) {
    std::stringstream ss{line};
    ss >> nsamples >> nclasses;
  }
  unsigned nsamples, nclasses;
};

struct LabeledRun {
  LabeledRun(const std::string& line) {
    std::stringstream ss{line};
    ss >> rep >> nsamples >> nclasses >> plabeled;
  }
  LabeledRun(const Run& run)
      : rep{run.rep},
        nsamples{run.nsamples},
        nclasses{run.nclasses},
        plabeled{run.plabeled} {}
  bool operator==(const LabeledRun& other) const {
    return rep == other.rep && nsamples == other.nsamples &&
           nclasses == other.nclasses && plabeled == other.plabeled;
  }
  unsigned rep, nsamples, nclasses;
  float plabeled;
};

struct EDSRun {
  EDSRun(const std::string& line) {
    std::stringstream ss{line};
    ss >> alpha >> lambda >> max_epochs;
  }
  double alpha, lambda;
  int max_epochs;
};

template <typename Key, typename Value>
class DumbMap {
 public:
  using ValueType = std::pair<Key, Value>;
  using Container = std::vector<ValueType>;
  using Iterator = typename Container::iterator;

  Iterator find(const Key& key) {
    for (auto it = values.begin(); it != values.end(); ++it)
      if (it->first == key) return it;
    return values.end();
  }

  std::pair<Iterator, bool> insert(const ValueType& value) {
    auto it = this->find(value.first);
    if (it != values.end()) return {it, false};
    values.push_back(value);
    return {values.end() - 1, true};
  }

  Iterator begin() { return values.begin(); }
  Iterator end() { return values.end(); }

 private:
  Container values;
};

static std::ostream& operator<<(std::ostream& os, const Run& run);
static std::vector<Run> read_specification(std::istream& is);

class Producer {
 public:
  Producer(std::vector<Run>&& runs) : runs{std::move(runs)} {}

  ~Producer() {
    for (auto& entry : sources) delete entry.second;
    for (auto& entry : networks) delete entry.second;
  }

  std::pair<eds::ssl::ProblemInput, std::string> get_if_not_empty() {
    std::lock_guard<std::mutex> lock{mutex};
    if (runs.empty()) return {{}, ""};
    Run run = runs.back();
    runs.pop_back();

    eds::ssl::ProblemInput input;

    input.alpha = run.alpha;
    input.lambda = run.lambda;
    input.max_epochs = run.max_epochs;

    std::stringstream ss;
    ss.precision(run.input_precision);
    ss << std::fixed << run.input_path << "labeled,rep=" << run.rep
       << ",nsamples=" << run.nsamples << ",nclasses=" << run.nclasses
       << ",plabeled=" << run.plabeled << ".tab";
    std::string labeled_path = ss.str();

    ss.str("");
    ss.clear();
    ss.precision(run.input_precision);
    ss << std::fixed << run.input_path << "y,nsamples=" << run.nsamples
       << ",nclasses=" << run.nclasses << ".tab";
    std::string y_path = ss.str();

    ss.str("");
    ss.clear();
    ss.precision(run.input_precision);
    ss << std::fixed << run.input_path << "nsamples=" << run.nsamples
       << ",p=" << run.p << ",q=" << run.q << ",sd=" << run.sd << ",k=" << run.k
       << ".net";
    std::string network_path = ss.str();

    NetworkRun network_run{run};
    LabeledRun labeled_run{run};

    decltype(sources)::Iterator source;
    if ((source = sources.find(labeled_run)) == sources.end()) {
      std::cerr << "[" << std::this_thread::get_id()
                << "] Generating source list from " << y_path << " and "
                << labeled_path << "..." << std::endl;

      auto Y = jules::read_table<double>(y_path).select(0);
      auto labeled = jules::read_table<double>(labeled_path).select(0);

      auto* lists = new eds::ssl::SourcesLists{{Y}, labeled};
      source = sources.insert({labeled_run, lists}).first;
    }

    decltype(networks)::Iterator network;
    if ((network = networks.find(network_run)) == networks.end()) {
      std::cerr << "[" << std::this_thread::get_id()
                << "] Generating graph from " << network_path << "..."
                << std::endl;
      auto* input_graph = new InputGraph{network_path};
      network = networks.insert({network_run, input_graph}).first;
    }

    input.sources = source->second;
    input.network = &network->second->graph;
    input.edge_vector = &network->second->edge_vector;
    input.edge_lists = &network->second->edge_lists;

    ss.str("");
    ss.clear();
    ss << run;

    return {input, ss.str()};
  }

 private:
  std::vector<Run> runs;
  std::mutex mutex;

  struct InputGraph {
    InputGraph(const std::string& path)
        : graph{igraph::Graph::ReadPajek(path).to_undirected()},
          edge_vector{graph},
          edge_lists(graph) {}
    igraph::Graph graph;
    igraph::EdgeVector edge_vector;
    igraph::EdgeLists edge_lists;
  };

  DumbMap<NetworkRun, InputGraph*> networks;
  DumbMap<LabeledRun, eds::ssl::SourcesLists*> sources;
};

class Consumer {
 public:
  Consumer(Producer* producer) : thread{Consumer::Consume, producer} {}
  void wait() { thread.join(); }

 private:
  static void Consume(Producer* producer) {
    unsigned executed = 0, skipped = 0, error = 0;
    eds::ssl::cpu::EdgeDynamicsSystem eds;

    while (true) {
      auto problem = producer->get_if_not_empty();
      if (problem.second.empty()) break;

      if (filesystem::exists(problem.second)) {
        std::cerr << "[" << std::this_thread::get_id() << "] Skipping "
                  << problem.second << std::endl;
        ++skipped;
        continue;
      }

      std::cerr << "[" << std::this_thread::get_id() << "] Executing "
                << problem.second << std::endl;

      try {
        eds.Initialize(&problem.first);
        while (!eds.IsFinished()) eds.Epoch();

        auto dynamics = eds.EdgeDominance();
        dynamics.save(problem.second, arma::arma_ascii);

        eds.Finalize();
        ++executed;
      } catch (std::exception& exc) {
        std::cerr << "[" << std::this_thread::get_id()
                  << "] Error when executing " << problem.second << ": "
                  << exc.what() << std::endl;
        ++error;
      }
    }

    std::cerr << "[" << std::this_thread::get_id() << "] " << executed
              << " processes executed successfully." << std::endl;
    std::cerr << "[" << std::this_thread::get_id() << "] " << error
              << " processes could not be executed." << std::endl;
    std::cerr << "[" << std::this_thread::get_id() << "] " << skipped
              << " processes skipped." << std::endl;
  }

  std::thread thread;
};

int main(int argc, char* argv[]) {
  if (argc != 2) throw std::runtime_error("Invalid arguments");
  std::ifstream input(argv[1]);
  std::cout << "Reading " << argv[1] << std::endl;
  Producer producer{read_specification(input)};

#ifndef NDEBUG
  unsigned ncores = 1;
#else
  unsigned ncores = std::thread::hardware_concurrency();
#endif  // NDEBUG

  std::cerr << "Initializing " << ncores << " threads..." << std::endl;
  std::vector<Consumer> consumers;
  for (unsigned i = 0; i < ncores; ++i) consumers.emplace_back(&producer);

  for (unsigned i = 0; i < ncores; ++i) consumers[i].wait();

  return 0;
}

std::ostream& operator<<(std::ostream& os, const Run& run) {
  os.precision(run.input_precision);
  return os << std::fixed << run.output_path
            << "dynamics,nsamples=" << run.nsamples << ",p=" << run.p
            << ",q=" << run.q << ",sd=" << run.sd << ",k=" << run.k
            << ",nclasses=" << run.nclasses << ",rep=" << run.rep
            << ",plabeled=" << run.plabeled << ",alpha=" << run.alpha
            << ",lambda=" << run.lambda << ",max_epochs=" << run.max_epochs
            << ".tab";
}

std::vector<Run> read_specification(std::istream& is) {
  std::cerr << "Reading experiment specification..." << std::endl;

  std::string line;
  std::unordered_map<std::string, std::vector<std::string>> sections;
  auto current_section = sections.end();

  while (std::getline(is, line)) {
    if (line[0] == '#') continue;

    if (line[0] == '[') {
      current_section =
          sections.insert({line.substr(1, line.find_first_of("]") - 1), {}})
              .first;
      continue;
    }

    if (current_section == sections.end())
      throw std::runtime_error{"invalid specification file: no section"};

    current_section->second.push_back(line);
  }

  unsigned precision = 2;
  if ((current_section = sections.find("precision")) != sections.end())
    precision = std::stoul(current_section->second.back());

  // Read path
  auto input_path = sections["input path"].back();
  auto output_path = sections["output path"].back();

  // Read networks
  std::vector<NetworkRun> network_runs;
  auto& network_section = sections["network"];
  std::transform(network_section.begin(), network_section.end(),
                 std::back_inserter(network_runs),
                 [](auto& line) -> NetworkRun { return line; });

  std::cerr << "Number of network configurations: " << network_runs.size()
            << std::endl;

  // Read y
  std::vector<YRun> y_runs;
  auto& y_section = sections["y"];
  std::transform(y_section.begin(), y_section.end(), std::back_inserter(y_runs),
                 [](auto& line) -> YRun { return line; });

  std::cerr << "Number of class configurations: " << y_runs.size() << std::endl;

  // Read labeled
  std::vector<LabeledRun> labeled_runs;
  auto& labeled_section = sections["labeled"];
  std::transform(labeled_section.begin(), labeled_section.end(),
                 std::back_inserter(labeled_runs),
                 [](auto& line) -> LabeledRun { return line; });

  // Read EDS parameters
  std::vector<EDSRun> eds_runs;
  auto& eds_section = sections["eds"];
  std::transform(eds_section.begin(), eds_section.end(),
                 std::back_inserter(eds_runs),
                 [](auto& line) -> EDSRun { return line; });

  std::cerr << "Number of initial labeled configurations: "
            << labeled_runs.size() << std::endl;

  std::vector<Run> result;
  for (const auto& network_run : network_runs) {
    for (const auto& y_run : y_runs) {
      for (const auto& labeled_run : labeled_runs) {
        for (const auto& eds_run : eds_runs) {
          if (network_run.nsamples != y_run.nsamples ||
              y_run.nsamples != labeled_run.nsamples)
            continue;

          if (y_run.nclasses != labeled_run.nclasses) continue;

          result.push_back(Run{input_path, output_path, network_run.nsamples,
                               network_run.p, network_run.q, network_run.sd,
                               network_run.k, y_run.nclasses, labeled_run.rep,
                               labeled_run.plabeled, eds_run.alpha,
                               eds_run.lambda, eds_run.max_epochs, precision});
        }
      }
    }
  }

  std::cerr << "Total unique configurations: " << result.size() << std::endl;

  return result;
}
