#include <cstdio>

#include <fstream>
#include <iomanip>
#include <string>

#include "core/debug.h"
#include "core/utility.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/printer.h"
#include "eds/semisupervised/specification.h"
#include "eds/semisupervised/problem_input.h"

namespace experiment {

namespace ssl {

void simulate(const eds::ssl::InputParameters& param,
              core::OutputPolicy& output) {
  using jules::column;
  using igraph::Graph;

  /* Read input files */
  std::stringstream result_file_s;
  result_file_s << output.prefix() << param << "\n";
  std::string result_file(result_file_s.str());

  if (output.contains(result_file)) {
    std::cout << "Skip " << result_file << "\n";
    return;
  }
  std::cout << result_file << "\n";
  auto Y = jules::read_table<double>(param.predictive.name).select(0);
  auto labeled = jules::read_table<double>(param.labeled.name).select(0);
  auto network = Graph::ReadPajek(param.network.name).to_undirected();
  igraph::EdgeLists edge_lists(network);
  igraph::EdgeVector edge_vector(network);
  eds::ssl::SourcesLists sources({Y}, labeled);

  /* Prepare algorithm input */
  eds::ssl::ProblemInput input;
  input.alpha = param.alg.alpha;
  input.lambda = param.alg.lambda;
  input.max_epochs = param.alg.max_epochs;
  input.sources = &sources;
  input.network = &network;
  input.edge_vector = &edge_vector;
  input.edge_lists = &edge_lists;

  /* Run */
  eds::ssl::cpu::EdgeDynamicsSystem eds;

  eds.Initialize(&input);
  while (!eds.IsFinished()) eds.Epoch();

  std::unique_ptr<std::stringstream> out(new std::stringstream);
  eds.EdgeDominance().save(*out.get(), arma::raw_ascii);
  output.push({result_file, std::move(out)});

  eds.Finalize();
}

}  // namespace ssl

}  // namespace experiment

int main(int argc, char* argv[]) {
  if (argc != 2) {
    printd(ERROR, "Specify a specification file.\n");
    fprintf(stderr, "Format: %s specification.json\n", argv[0]);
    return 1;
  }
  std::ifstream json(argv[1]);
  if (!json.is_open()) {
    printd(ERROR, "Cannot read specification file.\n");
    fprintf(stderr, "Format: %s specification.json\n", argv[0]);
    return 1;
  }
  eds::ssl::Specification spec(json);
  json.close();

  std::size_t i = 1, total = spec.inputs().size();
  for (const auto& input : spec.inputs()) {
    fprintf(stderr, "Running %zd/%zd\n", i++, total);
    experiment::ssl::simulate(input, spec.output());
  }
  return 0;
}
