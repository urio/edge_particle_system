#include <iostream>

#include <armadillo>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/tss.hpp>

#include "eds/semisupervised/classification.h"
#include "eds/semisupervised/problem_input.h"
#include "eds/semisupervised/cpu_eds.h"
#include "eds/semisupervised/specification.h"
#include "eds/semisupervised/flyweight/sources.h"
#include "core/flyweight/network.h"

namespace experiment {

namespace ssl {

void simulate_and_classify(const eds::ssl::InputParameters& input,
                           const std::vector<int>& neighborhood_orders,
                           core::OutputPolicy& output) {
  /* Get result filename and check whether we can skip it */
  std::string simulation_filename(input.simulation_name(output.prefix()));
  std::string classification_filename(
      input.classification_name(output.prefix()));

  bool simulate = !output.contains(simulation_filename);
  bool classify = !output.contains(classification_filename);

  if (!simulate && !classify) return;

  std::cerr << ".";
  // std::cerr << simulation_filename << std::endl;

  /* Prepare algorithm input */
  using core::flyweight::Network;
  using eds::ssl::flyweight::Sources;
  Network network(input.network);
  Sources sources(std::make_pair(input.predictive, input.labeled));

  eds::ssl::ProblemInput problem;
  problem.alpha = input.alg.alpha;
  problem.lambda = input.alg.lambda;
  problem.max_epochs = input.alg.max_epochs;
  problem.sources = sources.get().sources_lists();
  problem.network = network.get().graph();
  problem.edge_vector = network.get().edge_vector();
  problem.edge_lists = network.get().edge_lists();

  /* Simulate */
  arma::mat edge_dominance;
  if (simulate) {
    static boost::thread_specific_ptr<eds::ssl::EdgeDynamicsSystem> instance;
    if (!instance.get()) {
      std::cout << "Loading EDS\n";
      instance.reset(new eds::ssl::cpu::EdgeDynamicsSystem);
    }
    instance->Initialize(&problem);
    while (!instance->IsFinished()) instance->Epoch();

    /* Write output to stream and send to the output manager */
    auto output_stream = std::make_unique<std::stringstream>();
    edge_dominance = instance->EdgeDominance();
    edge_dominance.save(*output_stream.get(), arma::raw_ascii);
    output.push({simulation_filename, std::move(output_stream)});
    instance->Finalize();
  }

  /* Classify */
  if (classify) {
    if (!simulate) edge_dominance.load(simulation_filename, arma::raw_ascii);
    eds::ssl::Classification cls(input, problem,
                                 *sources.get().predictive_factors(),
                                 *sources.get().labeled(), edge_dominance);
    cls.Prepare();
    const size_t V = problem.vcount();
    const size_t O = neighborhood_orders.size();
    arma::mat cls_result(1 + V, O);
    for (size_t o = 0; o < O; ++o) {
      auto order = neighborhood_orders[o];
      cls.ClassifyUncertainVertices(order);
      cls_result(0, o) = order;
      cls_result(arma::span(1, V), o) = arma::reshape(cls.result(), V, 1);
      DEBUG(std::cerr << "Accuracy: " << cls.accuracy() << "\n");
    }
    auto cls_output_stream = std::make_unique<std::stringstream>();
    cls_result.save(*cls_output_stream.get(), arma::raw_ascii);
    output.push({classification_filename, std::move(cls_output_stream)});
  }
}

}  // namepace ssl

}  // namespace experiment

static void help(int, char* argv[]) {
  fprintf(stderr, "Format: %s specification.json [NTHREADS]\n", argv[0]);
  exit(1);
}

int main(int argc, char* argv[]) {
  if (argc < 2 || argc > 3) help(argc, argv);
  std::ifstream json(argv[1]);
  if (!json.is_open()) help(argc, argv);
  eds::ssl::Specification spec(json);
  json.close();

  const int nthreads = argc > 2 ? std::stoi(argv[2]) : -1;

  fprintf(stderr, "Queueing... \n");
  std::size_t queued = 0, total = spec.inputs().size();
  std::size_t simulations = 0, classifications = 0;
  core::OutputPolicy& output = spec.output();
  const auto& nei_orders = spec.neighborhood_orders();
  boost::asio::io_service io_service;
  for (const auto& input : spec.inputs()) {
    if (!input.validate()) continue;
    std::string simul_file(input.simulation_name(output.prefix()));
    std::string class_file(input.classification_name(output.prefix()));
    bool simulate = !output.contains(simul_file);
    bool classify = !output.contains(class_file);
    if (simulate || classify) {
      io_service.post([&]() {
        experiment::ssl::simulate_and_classify(input, nei_orders, output);
      });
      queued++;
      if (simulate) simulations++;
      if (classify) classifications++;
    }
  }
  fprintf(stderr,
          "Queued %zu/%zu jobs. "
          "(simulations=%zu, classifications=%zu. One job may do both tasks)\n",
          queued, total, simulations, classifications);

  boost::thread_group pool;
  unsigned int cores = nthreads > 0 ? nthreads : boost::thread::hardware_concurrency();
  while (cores--)
    pool.create_thread(boost::bind(&boost::asio::io_service::run, &io_service));

  pool.join_all();
  return 0;
}
