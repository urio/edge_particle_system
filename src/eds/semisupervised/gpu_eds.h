#ifndef EDS_SSL_GPU_H_
#define EDS_SSL_GPU_H_

#include "./cpu_eds.h"
#include "./gpu_eds_op.h"

namespace eds {

namespace ssl {

namespace gpu {

class EdgeDynamicsSystem : public ssl::cpu::EdgeDynamicsSystem {
 public:
  virtual void Initialize(const ProblemInput *input);
  //  virtual void Epoch();
  virtual void Finalize();

 private:
  // void ProcessClass(std::size_t k);
  void ParticleMovement();

  EDS_Operations op;
};

}  // namespace gpu

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_GPU_H_
