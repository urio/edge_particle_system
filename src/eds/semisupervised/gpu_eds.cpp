#include "./gpu_eds.h"

#include <cmath>

#include <armadillo>

#include "core/debug.h"

namespace eds {

namespace ssl {

namespace gpu {

void EdgeDynamicsSystem::Initialize(const ProblemInput* input) {
  ssl::cpu::EdgeDynamicsSystem::Initialize(input);
  printd(INFO, "Loading GPU Edge Dynamics System.\n");
  op.Allocate(dynamics_.N_total);
}

void EdgeDynamicsSystem::Finalize() {
  op.Release();
  ssl::cpu::EdgeDynamicsSystem::Finalize();
}

void EdgeDynamicsSystem::ParticleMovement() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& P = dynamics_.probabilities;
  arma::rowvec& G = dynamics_.generated;
  /* Movement */
  cls.current_dominance = diagmat(cls.no_particles) * P;
  // op.n_current(cls.no_particles, P, cls.current_dominance.memptr());
  // cls.no_particles = cls.no_particles * P + G;
  op.movement(P, G, cls.no_particles.memptr());
  // cls.no_particles.raw_print("no_particles");
  cls.total_dominance += cls.current_dominance;
  // cls.no_particles.raw_print();
  // cls.total_dominance.raw_print();
}

}  // namespace gpu

}  // namespace ssl

}  // namespace eds
