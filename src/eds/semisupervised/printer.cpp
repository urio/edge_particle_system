#include "./printer.h"

#include <cstdio>

#include "core/debug.h"

namespace eds {

namespace ssl {

void Printer::update() {
  using std::fprintf;

  const auto& dynamics = subject()->dynamics();

  fprintf(stderr, "\n" BAKWHT BLDBLK "Epoch %3.0lf" TXTRST, dynamics.epoch);
  fprintf(stderr, "\n\n\n");

  for (std::size_t k = 0; k < dynamics.classes; ++k) {
    fprintf(stderr, BAKRED BLDWHT "Class #%zu" TXTRST "\n", k);
    print_class(dynamics.data[k]);
  }
}

void Printer::print_class(const struct ClassDynamics& state) const {
  std::fprintf(stderr, TXTYLW "Number of particles:" TXTRST "\n");
  state.no_particles.raw_print();
  std::fprintf(stderr, TXTBLU "Walking matrix:" TXTRST "\n");
  state.walking.print();
  std::fprintf(stderr, TXTGRN "Current dominance:" TXTRST "\n");
  state.current_dominance.print();
  std::fprintf(stderr, TXTRED "Total dominance:" TXTRST "\n");
  state.total_dominance.print();
}

}  // namespace ssl

}  // namespace eds
