#ifndef EDS_SSL_H_
#define EDS_SSL_H_

#include <cstddef>

#include <vector>

#include <armadillo>
#include <jules/dataframe/column.hpp>
#include <jules/dataframe/dataframe.hpp>

#include "core/categorical.h"
#include "core/edge_vector.h"
#include "core/edge_lists.h"
#include "core/observer.h"

#include "./problem_input.h"

namespace eds {

namespace ssl {

struct ClassDynamics {
  arma::mat walking;            // P
  arma::rowvec no_particles;    // n
  arma::mat current_dominance;  // Ν
  arma::mat total_dominance;    // Δ
  double n0 = 0.0;                    // number of particles at time 0
  arma::rowvec rho;             // dist. of new particles
};

struct EDSDynamics {
  /* Current epoch, or last epoch executed (if none is being executed) */
  double epoch = 0.0;
  /* State dynamics for each class */
  std::vector<ClassDynamics> data;

  std::size_t classes = 0;  // Number of classes, same as |data.size()|
  std::size_t vcount = 0;   // Number of vertices, same as |network.vcount()|

  /* Support variables. We allocate only once and reuse the memory space. */
  arma::mat N_total;
  arma::mat N_current_normalized;
  arma::mat probabilities;
  arma::rowvec generated;  // Newly generated particles
  arma::rowvec new_no_particles;
};

class EdgeDynamicsSystem;

using Observer = core::Observer<EdgeDynamicsSystem>;

class EdgeDynamicsSystem {
 public:
  EdgeDynamicsSystem() = default;
  virtual ~EdgeDynamicsSystem() = default;

  virtual void Initialize(const ProblemInput *input);
  virtual void Epoch() = 0;
  virtual bool IsFinished() const noexcept { return finished(); }
  virtual const EDSDynamics &dynamics() const { return dynamics_; };
  virtual void Finalize() = 0;

  /* Register an |observer|, which will be called by notify(). */
  void attach(Observer *observer);
  /* Call all attached observers. */
  void notify();

  jules::column Result() const;
  /* Columns: i, j, class 1, class 2, ..., class n */
  arma::mat EdgeDominance() const;

  void set_finished(bool finished) { finished_ = finished; }
  bool finished() const { return finished_; }

  const ProblemInput *input() const { return input_; }

  /* Controls performance and safe memory initialization */
  bool safe_memory() const { return safe_memory_; }
  void set_safe_memory(bool safe) { safe_memory_ = safe; }

 protected:
  EDSDynamics dynamics_;

 private:
  void AllocateData();
  void InitData();
  void InitWalkingMatrix();
  void InitInitialState();
  void InitDominance();
  void InitDistributionOfNewParticles();

  bool safe_memory_ = true;
  bool finished_ = false;
  const ProblemInput *input_ = nullptr;
  std::vector<Observer *> observers_;
};

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_H_
