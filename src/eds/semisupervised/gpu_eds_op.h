#ifndef EDS_SSL_GPU_OP_H_
#define EDS_SSL_GPU_OP_H_

#include <armadillo>

namespace eds {

namespace ssl {

namespace gpu {

struct EDS_Internals;

class EDS_Operations {
 public:
  EDS_Operations();
  ~EDS_Operations();

  /* Allocate required space for operations. |spec| is only used to obtain the
   * required amount of resources. */
  void Allocate(const arma::mat& spec);

  void movement(const arma::mat& P, const arma::rowvec& G,
                double* no_particles);

  void Release();

 private:
  struct EDS_Internals* int_;
};

}  // namespace gpu

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_GPU_OP_H_
