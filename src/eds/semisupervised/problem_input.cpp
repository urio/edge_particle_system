#include "./problem_input.h"

#include <cstddef>

#include <stdexcept>

#include "core/debug.h"

namespace eds {

namespace ssl {

bool ProblemInput::Validate() const {
  bool valid = true;

  if (network == nullptr) {
    printd(ERROR, "Input network cannot be NULL. (ProblemInput.network)\n");
    valid = false;
  }

  if (edge_vector == nullptr) {
    printd(ERROR,
           "Edge vector cannot be NULL. Build it manually together with "
           "the input network. (ProblemInput.edge_vector)\n");
    valid = false;
  }

  if (edge_lists == nullptr) {
    printd(ERROR,
           "Edge lists cannot be NULL. Build it manually together with "
           "the input network. (ProblemInput.edge_lists)\n");
    valid = false;
  }

  if (sources == nullptr) {
    printd(ERROR,
           "Lists of sources cannot be NULL. Build it manually together "
           "with the input network. (ProblemInput.sources)\n");
    valid = false;
  }

  if (!valid) return valid;

  if (network->vcount() == 0) {
    printd(ERROR, "Input network cannot be empty.\n");
    valid = false;
  }
  if (network->is_directed()) {
    printd(ERROR, "Input network must be undirected.\n");
    valid = false;
  }
  if (sources->size() < 2) {
    printd(ERROR, "There must be at least one two classes in the problem.\n");
    valid = false;
  }
  if (initial_no_particles && initial_no_particles->size() > 0) {
    if (initial_no_particles->size() != sources->size()) {
      printd(ERROR, "Initial state: %zu initial states instead of %zu.\n",
             initial_no_particles->size(), sources->size());
      valid = false;
    }
    for (std::size_t k = 0; k < initial_no_particles->size(); ++k) {
      std::size_t V = static_cast<std::size_t>(network->vcount());
      std::size_t size = initial_no_particles[k].size();
      if (V != size) {
        printd(ERROR,
               "Invalid initial state for class id %zd: "
               "only %zd values. Expected: %zd.\n",
               k, size, V);
        valid = false;
        break;
      }
    }
  }
  for (const auto& src : *sources) {
    if (src.empty()) {
      printd(ERROR, "There must be at least one source per class.\n");
      valid = false;
      break;
    }
  }
  if (lambda < 0.0 || lambda > 1.0) {
    printd(ERROR, "Incorrect parameter interval. λ = %lf ∉ [0, 1]\n", lambda);
    valid = false;
  }
  if (alpha < 0.0) {
    printd(ERROR, "Incorrect parameter interval. α = %lf ∉ [0, ∞)\n", alpha);
    valid = false;
  }
  if (epsilon < 0.0) {
    printd(ERROR, "Incorrect parameter interval. ε = %lf ∉ [0, ∞)\n", epsilon);
    valid = false;
  }
  if (!valid) throw std::runtime_error("Invalid problem input");
  return valid;
}

}  // namespace ssl

}  // namespace eds
