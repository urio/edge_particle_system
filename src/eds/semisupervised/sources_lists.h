#ifndef EDS_SSL_SOURCES_LISTS_H
#define EDS_SSL_SOURCES_LISTS_H

#include <set>
#include <string>
#include <vector>

#include <jules/dataframe/column.hpp>

#include "core/categorical.h"

namespace eds {

namespace ssl {

/* Manage the list of labeled vertices (sources) of a dynamical system. */
class SourcesLists : public std::vector<std::set<std::size_t>> {
 public:
  SourcesLists() = default;
  ~SourcesLists() = default;

  template <typename CategoryValueT = double>
  SourcesLists(const core::Categorical<CategoryValueT> &predictive,
               const jules::column &labeled_indices) {
    Initialize(predictive, labeled_indices);
  }

  template <typename CategoryValueT = double>
  void Initialize(const core::Categorical<CategoryValueT> &predictive,
                  const jules::column &labeled_indices) {
    clear();
    resize(predictive.size());
    for (auto index : labeled_indices.view<double>()) {
      std::size_t vertex_index = static_cast<std::size_t>(index);
      std::size_t category = predictive.at(index);
      at(category).insert(vertex_index);
    }
  }

  /* Whether |vertex_index| is a source of particles for |list_index| class */
  bool is_source(std::size_t list_index, std::size_t vertex_index) const {
    const auto &lst = at(list_index);
    return lst.find(vertex_index) != lst.end();
  }

  /* Whether |vertex_index| is a source of particles that is _not_ for
   * |list_index| class */
  bool is_rival(std::size_t list_index, std::size_t vertex_index) const {
    for (std::size_t i = 0; i < size(); ++i) {
      if (i != list_index) {
        const auto &lst = at(i);
        if (lst.find(vertex_index) != lst.end()) return true;
      }
    }
    return false;
  }
};

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_SOURCES_LISTS_H
