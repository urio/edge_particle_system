#ifndef EDS_SSL_FLYWEIGHT_SOURCES_H_
#define EDS_SSL_FLYWEIGHT_SOURCES_H_

#include <utility>

#include <boost/flyweight.hpp>
#include <boost/flyweight/key_value.hpp>
#include <boost/flyweight/no_tracking.hpp>
#include <boost/flyweight/simple_locking.hpp>
#include <jules/dataframe/column.hpp>

#include "core/debug.h"
#include "core/file_information.h"
#include "core/utility.h"

#include "../sources_lists.h"

namespace eds {

namespace ssl {

namespace flyweight {

using FilePair = std::pair<core::FileInformation, core::FileInformation>;

class sources_t {
 public:
  sources_t(const FilePair& files)
      : files_(files),
        predictive_(jules::read_table<double>(files.first.name).select(0)),
        predictive_factors_(predictive_),
        labeled_(jules::read_table<double>(files.second.name).select(0)) {
    sources_ = SourcesLists{predictive_factors_, labeled_};
    printd(INFO, "Loaded sources %s %s\n", files_.first.name.c_str(),
           files_.second.name.c_str());
  }

  sources_t(const sources_t& network) = delete;  // : files_(network.file_) {}

  ~sources_t() {
    printd(INFO, "Releasing sources %s %s\n", files_.first.name.c_str(),
           files_.second.name.c_str());
  }

  const FilePair& files() const { return files_; }

  SourcesLists const* sources_lists() const { return &sources_; }
  jules::column const* predictive() const { return &predictive_; }
  core::Categorical<double> const* predictive_factors() const {
    return &predictive_factors_;
  }
  jules::column const* labeled() const { return &labeled_; }

 private:
  const FilePair files_;
  SourcesLists sources_;
  jules::column predictive_;
  core::Categorical<double> predictive_factors_;
  jules::column labeled_;
};

struct sources_files_info_extractor {
  const FilePair& operator()(const sources_t& sources) const {
    return sources.files();
  }
};

struct sources_tag {};

typedef boost::flyweight<boost::flyweights::key_value<
                             FilePair, sources_t, sources_files_info_extractor>,
                         boost::flyweights::tag<sources_tag>,
                         boost::flyweights::no_tracking,
                         boost::flyweights::simple_locking> Sources;

}  // namespace flyweight

}  // namespace ssl

}  // namespace eds

#endif  // ifndef EDS_SSL_FLYWEIGHT_SOURCES_H_
