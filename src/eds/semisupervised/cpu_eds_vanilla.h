#ifndef EDS_SSL_CPU_VANILLA_H_
#define EDS_SSL_CPU_VANILLA_H_

#include "./eds.h"

namespace eds {

namespace ssl {

namespace cpu {

class EdgeDynamicsSystemVanilla : public ssl::EdgeDynamicsSystem {
 public:
  EdgeDynamicsSystemVanilla();
  ~EdgeDynamicsSystemVanilla();

  virtual void Initialize(const ProblemInput *input);
  virtual void Epoch();
  virtual void Finalize();

 protected:
  virtual void ComputeNtotal();
  virtual void ProcessClass();

  virtual void ComputeNnorm();
  virtual void ComputePmatrix();
  virtual void ParticleProduction();
  virtual void ParticleMovement();

  std::size_t current_class_;
};

}  // namespace cpu

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_CPU_VANILLA_H_
