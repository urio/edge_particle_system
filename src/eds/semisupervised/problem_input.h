#ifndef PROBLEM_INPUT_H
#define PROBLEM_INPUT_H

#include <vector>

#include <armadillo>

#include <igraphpp/igraph.hpp>

#include "core/edge_lists.h"
#include "core/edge_vector.h"

#include "./sources_lists.h"

namespace eds {

namespace ssl {

struct ProblemInput {
  /* A simple, unweighted, undirected graph.
   * This is the complex network that determines the interaction of
   * particles during the particle competition. */
  igraph::Graph const *network = nullptr;
  /* Vector of edge pairs. Undirected, single pair for (i, j) and (j, i) */
  igraph::EdgeVector const *edge_vector = nullptr;
  /* Vector of edge pairs. Directed. For (i, j), i and j appear in their
   * respective edge lists. */
  igraph::EdgeLists const *edge_lists = nullptr;
  /* Initial state of the system. If empty, the default initial state
   * mechanism is triggered. */
  std::vector<arma::colvec> *initial_no_particles = nullptr;
  /* Source vertices (in semi-supervised learning, labeled data points) are
   * defined apart from the network.
   * |sources| has a vector of indices for each class.
   * sources[i] contains the indices for class i.
   * sources.size() is the number of available classes in the system. */
  ssl::SourcesLists const *sources = nullptr;
  /* λ ∊ [0, 1] is the competition parameter.
   * It influences the survival of a particle over the competition process.
   * λ = 0 — no competition and maximum survival.
   * λ = 1 — maximum competition. */
  double lambda = 1.0;
  /* α ∊ [0, ∞) is the production decay parameter.
   * It controls how many particles are fed to the system over the system
   * evolution.
   * α = 0 — no decay, sources keep the same amout of the initial number of
   *         particles.
   * α = ∞ — no production, sources do not generate new particles. */
  double alpha = 1.0;
  /* ε > 0 is TODO... */
  double epsilon = 1e-10;
  /* Stop criterion.
   * Maximum number of epochs before finishing a simulation. */
  int max_epochs = 1000;
  /* Stop criterion.
   * Halt the system evolution if the system reaches a state with no active
   * particles. */
  bool halt_if_empty = false;
  /* When |halt_if_empty| is enabled, the criterion to consider the system
   * as empty is to have fewer than |minimum_number_of_particles| active
   * particles. */
  double minimum_number_of_particles = 1.0;

  std::size_t classes() const { return sources->size(); }
  std::size_t vcount() const {
    return static_cast<std::size_t>(network->vcount());
  }
  std::size_t ecount() const {
    return static_cast<std::size_t>(network->ecount());
  }

  /* Check setup consistency */
  bool Validate() const;
};

}  // namespace ssl

}  // namespace eds

#endif  // PROBLEM_INPUT_H
