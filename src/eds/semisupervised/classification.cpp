#include "./classification.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <queue>

#include <armadillo>

#include "core/utility.h"

namespace eds {

namespace ssl {

Classification::Classification(const InputParameters& input,
                               const ProblemInput& problem,
                               const core::Categorical<double>& predictive,
                               const jules::column& labeled,
                               const arma::mat& edge_dominance)
    : input_(input),
      problem_(problem),
      predictive_(predictive),
      labeled_(labeled),
      edge_dominance_(edge_dominance) {
  ValidateEdgeDominanceAndNetwork();
}

Classification::~Classification() {}

void Classification::Prepare() {
  const auto& ed = edge_dominance_.cols(2, edge_dominance_.n_cols - 1);
  edge_winners_ = arma::max_col(ed, arma::TieMethod::Random);
  VerticesAtEndpoints();
}

void Classification::ValidateEdgeDominanceAndNetwork() const {
  using arma::span;
  const auto M = edge_dominance_.n_rows;

  for (arma::mat::size_type row = 0; row < M; ++row) {
    int first = edge_dominance_(row, 0);
    int second = edge_dominance_(row, 1);
    auto e = problem_.network->eid(first, second);
    if (e != static_cast<int>(row)) {
      printd(ERROR,
             "Edge eid list do not match! Not the same network used on "
             "simulation?\n");
      throw std::runtime_error("Edge eid list do not match on classification");
    }
  }
}

void Classification::VerticesAtEndpoints() {
  constexpr double kTagUnmet = -1.0;
  constexpr double kTagUncertain = -2.0;
  const auto V = problem_.vcount();

  result_.set_size(V);
  result_.fill(kTagUnmet);

  uncertain_vertices_.clear();
  uncertain_vertices_.reserve(V / 4);  // Hint of maximum size

  /* result_[i] is tagged as follows:
   * kTagUnmet = this vertex was not at the endpoint of any edges we have
   *             examined so far.
   * kTagUncertain = this vertex is at the endpoints of edge dominated by
   *                 different classes, making it uncertain to which class it
   *                 should be classified.
   * Otherwise: the i-th position has the index of the class that dominates
   * edges connected to the vertex, making it the classification result for that
   * vertex.
   */
  auto tag_or_classify_vertex = [&](std::size_t vertex_id, int k) {
    auto& tag = result_[vertex_id];
    if (tag == kTagUnmet) {
      tag = k;
    } else {
      if (tag != kTagUncertain && tag != k) {
        uncertain_vertices_.push_back(vertex_id);
        tag = kTagUncertain;
      }
    }
  };

  /* Go through each edge and process the vertices at the endpoints. */
  for (std::size_t i = 0; i < problem_.edge_vector->size(); ++i) {
    const auto& edge = problem_.edge_vector->at(i);
    const auto k = edge_winners_[i];
    tag_or_classify_vertex(edge.first, k);
    tag_or_classify_vertex(edge.second, k);
  }

#ifndef NDEBUG  // Debug mode
  for (std::size_t v = 0; v < result_.size(); ++v) {
    if (result_[v] == kTagUnmet) {
      printd(WARN,
             "Isolated vertex %zd is not classified, because there "
             "are no edges connected to it. (value = %lf)\n",
             v, result_[v]);
    }
  }
#endif
}

void Classification::ClassifyUncertainVertices(int neighborhood_order) {
  std::size_t K = problem_.classes();
  igraph::Vector pairs;

  /* Count of edges in a neighborhood by winners */
  arma::rowvec nei_edge_density(K, arma::fill::none);

  for (const auto vertex : uncertain_vertices_) {
    for (std::size_t k = 0; k < K; ++k) {
      nei_edge_density[k] =
          static_cast<double>(count_edges(vertex, k, neighborhood_order));
    }
    result_[vertex] = max_rowvec(nei_edge_density, arma::TieMethod::Random);
  }
}

double Classification::accuracy() const {
  std::size_t V = problem_.vcount();
  arma::rowvec cat(V);
  for (std::size_t r = 0; r < V; ++r) {
    cat[r] = predictive_.at(r);
  }

  const double matches = sum(cat == result_) - labeled_.size();
  const double total = V - labeled_.size();
  // std::cerr << "Accuracy: " << matches << "/" << total << " = "
  //           << matches / total << std::endl;
  return matches / total;
}

unsigned int Classification::count_edges(int root, int k, int order) const {
  const auto& network = *problem_.network;
  const auto& edge_lists = *problem_.edge_lists;
  const auto& eids = edge_lists.eids();
  std::queue<int> Q;
  unsigned int result = 0;

  std::vector<int> distance(network.vcount(), order + 1);
  std::vector<bool> edge_discovered(network.ecount(), false);

  distance[root] = 0;
  Q.push(root);
  while (!Q.empty()) {
    const auto curr = Q.front();
    Q.pop();
    const auto curr_distance = distance[curr];
    const auto& eids_neighbors = eids[curr];
    auto nei = edge_lists[curr].begin();
    for (auto e : eids_neighbors) {
      if (edge_winners_[e] == k && edge_discovered[e] == false) {
        const auto to = *nei;
        if (distance[to] > curr_distance) {
          distance[to] = curr_distance + 1;
        }
        if (distance[to] <= order) {
          ++result;
          if (distance[to] > curr_distance) {
            Q.push(to);
          }
        }
      }
      edge_discovered[e] = true;
      ++nei;
    }
  }
  return result;
}

}  // namespace ssl

}  // namespace eds
