#include "./gpu_eds_op.h"

#include <armadillo>

#include <cuda.h>
#include <cublas_v2.h>

#include "core/cutil.cuh"
#include "core/debug.h"

namespace eds {

namespace ssl {

namespace gpu {

struct EDS_Internals {
  double *dev_P;

  double *dev_no_particles;
  double *dev_new_no_particles;
  std::size_t no_particles_size;

  std::size_t mat_size;
  std::size_t dim;
  cublasHandle_t cublasHandle;

  EDS_Internals()
      : dev_P(NULL), dev_no_particles(NULL), dev_new_no_particles(NULL) {
    cublasSafeCall(cublasCreate(&cublasHandle));
  }

  ~EDS_Internals() {
    if (dev_P != NULL) cudaSafeCall(cudaFree(dev_P));
    if (dev_no_particles != NULL) cudaSafeCall(cudaFree(dev_no_particles));
    if (dev_new_no_particles != NULL)
      cudaSafeCall(cudaFree(dev_new_no_particles));
    cublasSafeCall(cublasDestroy(cublasHandle));
    printd(INFO, "Op internals destroyed.\n");
  }
};

EDS_Operations::EDS_Operations() {
  int_ = new EDS_Internals;
  printd(INFO, "Op initiated.\n");
}

EDS_Operations::~EDS_Operations() { delete int_; }

void EDS_Operations::Allocate(const arma::mat &spec) {
  std::size_t total = spec.size() * sizeof(double);
  std::size_t total_no = spec.n_cols * sizeof(double);
  int_->no_particles_size = total_no;
  printd(TESTING, "Allocating operation resources (Total: %zu bytes).\n",
         total);
  int_->mat_size = total;
  int_->dim = spec.n_rows;
  if (spec.n_rows != spec.n_cols) {
    printd(ERROR, "Expecting squared matrix. Received size(%zu, %zu).\n",
           spec.n_rows, spec.n_cols);
    throw std::runtime_error("Expected squared matrix");
  }
  cudaSafeCall(cudaMalloc(&int_->dev_P, total));
  cudaSafeCall(cudaMalloc(&int_->dev_no_particles, total_no));
  cudaSafeCall(cudaMalloc(&int_->dev_new_no_particles, total_no));
}

void EDS_Operations::movement(const arma::mat &P, const arma::rowvec &G,
                              double *no_particles) {
  cudaSafeCall(cudaMemcpy(int_->dev_P, P.memptr(), int_->mat_size,
                          cudaMemcpyHostToDevice));
  cudaSafeCall(cudaMemcpy(int_->dev_no_particles, no_particles,
                          int_->no_particles_size, cudaMemcpyHostToDevice));
  cudaSafeCall(cudaMemcpy(int_->dev_new_no_particles, G.memptr(),
                          int_->no_particles_size, cudaMemcpyHostToDevice));

  double alpha = 1.0;
  double beta = 1.0;
  int N = static_cast<int>(int_->dim);
  cublasSafeCall(                              // y = α op(A) x + β y
      cublasDgemv(int_->cublasHandle,          // cuBLAS handle
                  CUBLAS_OP_T,                 // op(A) operation
                  N,                           // rows of A
                  N,                           // cols of A
                  &alpha,                      // alpha
                  int_->dev_P,                 // A
                  N,                           // leading dim of A
                  int_->dev_no_particles,      // x
                  1,                           // incx, stride of x
                  &beta,                       // beta
                  int_->dev_new_no_particles,  // y
                  1                            // incy, stride of y
                  ));

  cudaSafeCall(cudaMemcpy(no_particles, int_->dev_new_no_particles,
                          int_->no_particles_size, cudaMemcpyDeviceToHost));
}

void EDS_Operations::Release() {}

}  // namespace gpu

}  // namespace ssl

}  // namespace eds
