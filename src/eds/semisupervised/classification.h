#ifndef EDS_SEMISUPERVISED_CLASSIFICATION_H_
#define EDS_SEMISUPERVISED_CLASSIFICATION_H_

#include <armadillo>

#include <vector>

#include "core/categorical.h"
#include "./specification.h"
#include "./problem_input.h"

namespace eds {

namespace ssl {

class Classification {
 public:
  Classification(const InputParameters& input, const ProblemInput& problem,
                 const core::Categorical<double>& predictive,
                 const jules::column& labeled, const arma::mat& edge_dominance);
  ~Classification();

  /* Classify vertices that are on normal neighborhood and find uncertain
   * vertices (those that have mixed dominated edges). */
  void Prepare();

  /* Classify or re-classify uncertain vertices using |neighborhood_order| as
   * local criterion for determining the correct class. */
  void ClassifyUncertainVertices(int neighborhood_order);

  double accuracy() const;
  const arma::rowvec& result() const { return result_; }

 private:
  /* Check whether edge dominance and network edge list match or not. */
  void ValidateEdgeDominanceAndNetwork() const;

  /* Populate list of vertices at endpoints by edge dominance.
   * This list is used to restrict BFS calls */
  void VerticesAtEndpoints();

  unsigned int count_edges(int root, int k, int order) const;

  /* Index of class that dominated each edge. */
  arma::rowvec edge_winners_;

  /* Classification result */
  arma::rowvec result_;

  /* Contains which vertices have edges dominanted by different classes.
   * These vertices may have significant entropy values, and we will need to
   * compute the graph neighborhood of them. */
  igraph::Vector uncertain_vertices_;

  /* Input data */
  const InputParameters& input_;
  const ProblemInput& problem_;
  const core::Categorical<double>& predictive_;
  const jules::column& labeled_;
  const arma::mat& edge_dominance_;
};

}  // namespace ssl

}  // namespace eds

#endif  // ifndef EDS_SEMISUPERVISED_CLASSIFICATION_H_
