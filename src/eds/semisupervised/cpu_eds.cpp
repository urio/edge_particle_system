#include "./cpu_eds.h"

#include <cmath>
#include <limits>

#include <armadillo>

#include "core/debug.h"

namespace eds {

namespace ssl {

namespace cpu {

EdgeDynamicsSystem::EdgeDynamicsSystem() : ssl::EdgeDynamicsSystem() {
  set_safe_memory(false);
  current_max_ = -std::numeric_limits<double>::infinity();
}

EdgeDynamicsSystem::~EdgeDynamicsSystem() {}

void EdgeDynamicsSystem::Initialize(const ProblemInput* input) {
  printd(TESTING, "Loading CPU Edge Dynamics System.\n");
  ssl::EdgeDynamicsSystem::Initialize(input);
  if (input->halt_if_empty) {
    printd(WARN, "“Halt if empty” mechanism has not been implemented.\n");
  }
  set_finished(false);
  dynamics_.epoch = 0.0;
}

void EdgeDynamicsSystem::Epoch() {
  // printd(TESTING, "Running epoch %.0lf.\n", dynamics().epoch);
  dynamics_.epoch += 1.0;
  double max_epochs = input()->max_epochs;
  if (max_epochs != 0.0 && dynamics().epoch > max_epochs) {
    printd(WARN,
           "Forcing to run after finishing. Epoch: %.0lf"
           " (max = %.0lf). Active particles: ? (min = %lf).\n",
           dynamics().epoch, max_epochs, input()->minimum_number_of_particles);
  }
  ComputeNtotal();
  for (std::size_t k = 0; k < dynamics().classes; ++k) {
    current_class_ = k;
    ProcessClass();
  }
  if (dynamics().epoch == max_epochs) set_finished(true);
}

void EdgeDynamicsSystem::ComputeNtotal() {
  for (std::size_t k = 0; k < dynamics().classes; ++k) {
    ClassDynamics& cls = dynamics_.data[k];
    const arma::mat& N = cls.current_dominance;

    if (k == 0) {
      for (const auto& e : *input()->edge_vector) {
        dynamics_.N_total(e.first, e.second) = 0;
        dynamics_.N_total(e.second, e.first) = 0;
      }
    }
    for (const auto& e : *input()->edge_vector) {
      auto sum = N(e.first, e.second) + N(e.second, e.first);
      cls.current_dominance(e.first, e.second) = sum;
      cls.current_dominance(e.second, e.first) = sum;
      const auto total = (dynamics_.N_total(e.first, e.second) += sum);
      dynamics_.N_total(e.second, e.first) = total;
    }
  }
}

void EdgeDynamicsSystem::ProcessClass() {
  ComputeNnorm();
  ComputePmatrix();
  ParticleProduction();
  ParticleMovement();
}

void EdgeDynamicsSystem::ComputeNnorm() {
  const double kResetProb = 1.0 / dynamics().classes;
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& N_norm = dynamics_.N_current_normalized;

  /* Compute normalized current_dominance */
  for (const auto& e : *input()->edge_vector) {
    std::size_t i = e.first, j = e.second;
    double total = dynamics_.N_total(i, j);
    if (total == 0.0) {
      N_norm(i, j) = kResetProb;
    } else {
      N_norm(i, j) = cls.current_dominance(i, j) / total;
    }

    total = dynamics_.N_total(j, i);
    if (total == 0.0) {
      N_norm(j, i) = kResetProb;
    } else {
      N_norm(j, i) = cls.current_dominance(j, i) / total;
    }
  }
}

void EdgeDynamicsSystem::ComputePmatrix() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& P = dynamics_.probabilities;
  const arma::mat& N_norm = dynamics().N_current_normalized;
  const double lambda = input()->lambda;
  const double kComplLambda = 1.0 - lambda;

  for (const auto& e : *input()->edge_vector) {
    const std::size_t i = e.first, j = e.second;
    P(i, j) = cls.walking(i, j) * (N_norm(i, j) * lambda + kComplLambda);
    P(j, i) = cls.walking(j, i) * (N_norm(j, i) * lambda + kComplLambda);
  }
}

void EdgeDynamicsSystem::ParticleProduction() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  const double alpha = input()->alpha;
  double g_first = std::floor(cls.n0 * std::pow(dynamics().epoch, -alpha));
  double g_second = sum(cls.no_particles);
  // printd(TESTING, "g.first = %lf, g.second = %lf\n", g_first, g_second);

  if (g_second > current_max_) current_max_ = g_second;

  arma::rowvec& G = dynamics_.generated;

  double k = g_first - g_second;
  if (k >= 0.0) {
    G = cls.rho * k;
  } else {
    G.zeros();
  }

  if (current_class_ == dynamics_.data.size() - 1) {
    if (current_max_ < input()->epsilon) set_finished(true);
    current_max_ = -std::numeric_limits<double>::infinity();
  }
}

void EdgeDynamicsSystem::ParticleMovement() {
  using std::size_t;

  ClassDynamics& cls = dynamics_.data[current_class_];
  const arma::mat& P = dynamics_.probabilities;
  const arma::rowvec& G = dynamics_.generated;
  arma::rowvec& new_no_particles = dynamics_.new_no_particles;
  const size_t V = dynamics().vcount;

  new_no_particles.zeros();
  for (size_t v = 0; v < V; ++v) {
    const double& particles = cls.no_particles[v];
    const auto& neighbors = input()->edge_lists->at(v);

    for (size_t nei : neighbors) {
      const double value = P(v, nei) * particles;
      cls.current_dominance(v, nei) = value;
      cls.total_dominance(v, nei) += value;
      new_no_particles[nei] += value;
    }
  }
  cls.no_particles = new_no_particles + G;
}

void EdgeDynamicsSystem::Finalize() {}

}  // namespace cpu

}  // namespace ssl

}  // namespace eds
