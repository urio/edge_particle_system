#include "./cpu_eds_vanilla.h"

#include <cmath>

#include <armadillo>

#include "core/debug.h"

namespace eds {

namespace ssl {

namespace cpu {

EdgeDynamicsSystemVanilla::EdgeDynamicsSystemVanilla()
    : ssl::EdgeDynamicsSystem() {
  set_safe_memory(true);
}
EdgeDynamicsSystemVanilla::~EdgeDynamicsSystemVanilla() {}

void EdgeDynamicsSystemVanilla::Initialize(const ProblemInput* input) {
  printd(INFO, "Loading CPU (Vanilla) Edge Dynamics System.\n");
  ssl::EdgeDynamicsSystem::Initialize(input);
  if (input->halt_if_empty) {
    printd(WARN, "“Halt if empty” mechanism has not been implemented.\n");
  }
  set_finished(false);
  dynamics_.epoch = 0.0;
}

void EdgeDynamicsSystemVanilla::Epoch() {
  printd(TESTING, "Running epoch %.0lf.\n", dynamics().epoch);
  dynamics_.epoch += 1.0;
  double max_epochs = input()->max_epochs;
  if (max_epochs != 0.0 && dynamics().epoch > max_epochs) {
    printd(WARN,
           "Forcing to run after finishing. Epoch: %.0lf"
           " (max = %.0lf). Active particles: ? (min = %lf).\n",
           dynamics().epoch, max_epochs, input()->minimum_number_of_particles);
  }
  ComputeNtotal();
  for (std::size_t k = 0; k < dynamics().classes; ++k) {
    current_class_ = k;
    ProcessClass();
  }
  if (dynamics().epoch == max_epochs) set_finished(true);
}

void EdgeDynamicsSystemVanilla::ComputeNtotal() {
  dynamics_.N_total.zeros();
  for (std::size_t k = 0; k < dynamics().classes; ++k) {
    ClassDynamics& cls = dynamics_.data[k];
    const arma::mat& N = cls.current_dominance;

    cls.current_dominance = N + trans(N);
    if (k == 0)
      dynamics_.N_total = cls.current_dominance;
    else
      dynamics_.N_total += cls.current_dominance;
  }
}

void EdgeDynamicsSystemVanilla::ProcessClass() {
  ComputeNnorm();
  ComputePmatrix();
  ParticleProduction();
  ParticleMovement();
}

void EdgeDynamicsSystemVanilla::ComputeNnorm() {
  const double kResetProb = 1.0 / dynamics().classes;
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& N_norm = dynamics_.N_current_normalized;

  /* Compute normalized current_dominance */
  N_norm.zeros();
  for (const auto& e : *input()->edge_vector) {
    std::size_t i = e.first, j = e.second;
    double total = dynamics_.N_total(i, j);
    if (total == 0.0) {
      N_norm(i, j) = kResetProb;
    } else {
      N_norm(i, j) = cls.current_dominance(i, j) / total;
    }

    total = dynamics_.N_total(j, i);
    if (total == 0.0) {
      N_norm(j, i) = kResetProb;
    } else {
      N_norm(j, i) = cls.current_dominance(j, i) / total;
    }
  }
}

void EdgeDynamicsSystemVanilla::ComputePmatrix() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& N_norm = dynamics_.N_current_normalized;
  arma::mat& P = dynamics_.probabilities;
  const double lambda = input()->lambda;

  P = cls.walking % (N_norm * lambda + (1.0 - lambda));
}

void EdgeDynamicsSystemVanilla::ParticleProduction() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  const double alpha = input()->alpha;
  double g_first = std::floor(cls.n0 * std::pow(dynamics().epoch, -alpha));
  double g_second = sum(cls.no_particles);
  printd(TESTING, "g.first = %lf, g.second = %lf\n", g_first, g_second);

  arma::rowvec& G = dynamics_.generated;

  G = cls.rho * (g_first - g_second);
  G.elem(find(G < 0.0)).fill(0.0);
}

void EdgeDynamicsSystemVanilla::ParticleMovement() {
  ClassDynamics& cls = dynamics_.data[current_class_];
  arma::mat& P = dynamics_.probabilities;
  arma::rowvec& G = dynamics_.generated;

  cls.current_dominance = diagmat(cls.no_particles) * P;
  cls.no_particles = cls.no_particles * P + G;
  cls.total_dominance += cls.current_dominance;
}

void EdgeDynamicsSystemVanilla::Finalize() {}

}  // namespace cpu

}  // namespace ssl

}  // namespace eds
