#include "./eds.h"

#include <cmath>
#include <cstddef>

#include <stdexcept>

#include "core/debug.h"

namespace eds {

namespace ssl {

void EdgeDynamicsSystem::Initialize(const ProblemInput* input) {
  if (!input->Validate())
    throw std::runtime_error("Invalid problem input setup.");
  input_ = input;
  AllocateData();
  InitData();
  printd(TESTING, "Basic initialization complete.\n");
}

void EdgeDynamicsSystem::attach(Observer* observer) {
  observers_.push_back(observer);
}

void EdgeDynamicsSystem::notify() {
  for (auto* observer : observers_) observer->update();
}

jules::column EdgeDynamicsSystem::Result() const {
  using std::size_t;
  size_t ecount = input()->ecount();
  size_t classes = dynamics().classes;
  auto ecol = jules::column{0.0, ecount};
  auto result = ecol.view<double>();

  for (size_t e = 0; e < ecount; ++e) {
    const auto& edge = input()->edge_vector->at(e);
    size_t winner = 0;
    double max_dominance =
        dynamics().data[0].total_dominance(edge.first, edge.second);
    bool tie = true;
    for (size_t k = 1; k < classes; ++k) {
      const ClassDynamics& cls = dynamics().data[k];
      double dominance = cls.total_dominance(edge.first, edge.second);
      double diff = dominance - max_dominance;
      if (fabs(diff) > 1e-8) tie = false;
      if (dominance > max_dominance) {
        winner = k;
        max_dominance = dominance;
      }
    }
    if (!tie) result[e] = winner + 1;
  }
  // Never visited edges have dominance equal to zero.
  return ecol;
}

arma::mat EdgeDynamicsSystem::EdgeDominance() const {
  using jules::column;
  using std::size_t;
  const size_t K = dynamics().classes;
  const size_t E = input()->edge_vector->size();

  arma::mat result(E, K + 2);
  for (size_t eid = 0; eid < E; ++eid) {
    const auto& e = input()->edge_vector->at(eid);
    result(eid, 0) = e.first;
    result(eid, 1) = e.second;
    for (size_t k = 0; k < K; ++k) {
      const auto& total_dominance = dynamics().data[k].total_dominance;
      result(eid, k + 2) = total_dominance(e.first, e.second) +
                           total_dominance(e.second, e.first);
    }
  }
  return result;
}

void EdgeDynamicsSystem::AllocateData() {
  /* At this point, dynamics() has not been updated with ProblemInput, and we
   * compare the input information with the dynamics.  This unitialized state
   * allows us to re-use memory that has already been allocated on a previous
   * run. */
  std::size_t classes = input()->classes();
  std::size_t vcount = input()->vcount();
  if (dynamics().classes == classes && dynamics().vcount == vcount) return;
  printd(TESTING,
         "Resizing data structures. classes=%zd vcount=%zd → "
         "classes=%zd vcount=%zd\n",
         dynamics().classes, dynamics().vcount, classes, vcount);
  if (safe_memory()) {
    dynamics_.data.resize(classes);
    dynamics_.N_total.resize(vcount, vcount);
    dynamics_.probabilities.resize(vcount, vcount);
    dynamics_.N_current_normalized.resize(vcount, vcount);
    dynamics_.generated.resize(vcount);
    dynamics_.new_no_particles.resize(vcount);
    for (std::size_t k = 0; k < classes; ++k) {
      ClassDynamics& cls = dynamics_.data[k];

      cls.walking.resize(vcount, vcount);
      cls.no_particles.resize(vcount);
      cls.current_dominance.resize(vcount, vcount);
      cls.total_dominance.resize(vcount, vcount);
      cls.rho.resize(vcount);
    }
  } else {
    dynamics_.data.resize(classes);
    dynamics_.N_total.set_size(vcount, vcount);
    dynamics_.probabilities.set_size(vcount, vcount);
    dynamics_.N_current_normalized.set_size(vcount, vcount);
    dynamics_.generated.set_size(vcount);
    dynamics_.new_no_particles.set_size(vcount);
    for (std::size_t k = 0; k < classes; ++k) {
      ClassDynamics& cls = dynamics_.data[k];

      cls.walking.set_size(vcount, vcount);
      cls.no_particles.set_size(vcount);
      cls.current_dominance.set_size(vcount, vcount);
      cls.total_dominance.set_size(vcount, vcount);
      cls.rho.set_size(vcount);
    }
  }
}

void EdgeDynamicsSystem::InitData() {
  dynamics_.classes = input()->classes();
  dynamics_.vcount = input()->vcount();
  InitWalkingMatrix();
  InitInitialState();
  InitDominance();
  InitDistributionOfNewParticles();
}

void EdgeDynamicsSystem::InitWalkingMatrix() {
  using std::size_t;
  const igraph::Graph& network = *input()->network;
  igraph::Vector vertex_degree = network.degree();

  if (dynamics_.data.size() != dynamics().classes) {
    printd(ERROR, "Really? WTF! dynamics> {.data.size=%zd} != {.classes=%zd}\n",
           dynamics_.data.size(), dynamics().classes);
  }
  for (size_t k = 0; k < dynamics().classes; ++k) {
    arma::mat& W = dynamics_.data[k].walking;
    if (safe_memory()) {
      W.zeros();
    }
    /* When |i| is rival of |k|, the entire row must be set to zero, otherwise,
     * the probability is computed and only |j| rivals are set to zero. */
    size_t i = 0;
    for (const auto& nei_i : *input()->edge_lists) {
      double probability = 0.0;
      if (!input()->sources->is_rival(k, i)) {
        probability = 1.0 / vertex_degree[i];
      }
      for (const size_t j : nei_i) {
        if (input()->sources->is_rival(k, j))
          W(i, j) = 0.0;
        else
          W(i, j) = probability;
      }
      i++;
    }
  }
}

void EdgeDynamicsSystem::InitInitialState() {
  if (input()->initial_no_particles != nullptr &&
      input()->initial_no_particles->size() != 0) {
    printd(TESTING, "Using input’s initial state.\n");
    for (std::size_t k = 0; k < dynamics().classes; ++k)
      dynamics_.data[k].no_particles = input()->initial_no_particles->at(k);
    return;
  }
  printd(TESTING, "Computing initial state.\n");
  const igraph::Graph& network = *input()->network;
  igraph::Vector degrees = network.degree() / (2.0 * network.ecount());
  for (size_t k = 0; k < dynamics().classes; ++k) {
    ClassDynamics& cls = dynamics_.data[k];
    arma::rowvec& vec = cls.no_particles;
    for (size_t i = 0; i < dynamics().vcount; ++i) {
      vec[i] = degrees[i];
    }
    cls.n0 = sum(vec);
    const double kExpected = 1.0;
    if (fabs(cls.n0 - kExpected) > 1e-6) {
      printd(ERROR, "Initial n° of particles is %lf. Expected: %lf\n", cls.n0,
             kExpected);
      throw std::runtime_error("Failed at computing initial state.");
    }
  }
}

void EdgeDynamicsSystem::InitDominance() {
  printd(TESTING, "Setting up dominance.\n");
  using std::size_t;
  const igraph::Graph& network = *input()->network;
  igraph::Vector degrees = network.degree();

  for (size_t k = 0; k < dynamics().classes; ++k) {
    ClassDynamics& cls = dynamics_.data[k];
    cls.total_dominance.zeros();
    arma::mat& N = cls.current_dominance;
    N.zeros();
    for (const auto& edge : *input()->edge_vector) {
      N(edge.first, edge.second) = 1.0;
      N(edge.second, edge.first) = 1.0;
    }
  }
}

void EdgeDynamicsSystem::InitDistributionOfNewParticles() {
  printd(TESTING, "Computing ρ distribution.\n");
  igraph::Vector vertex_degrees = input()->network->degree();
  for (size_t k = 0; k < dynamics().classes; ++k) {
    arma::rowvec& vec = dynamics_.data[k].rho;
    vec.zeros();
    for (size_t i = 0; i < dynamics().vcount; ++i) {
      if (input()->sources->is_source(k, i)) vec[i] = vertex_degrees[i];
    }
    vec /= sum(vec);
  }
}

}  // namespace ssl

}  // namespace eds
