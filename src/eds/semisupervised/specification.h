#ifndef EDS_SSL_SPECIFICATION_H_
#define EDS_SSL_SPECIFICATION_H_

#include <array>
#include <string>
#include <map>

#include "core/specification.h"
#include "core/file_information.h"

namespace eds {

namespace ssl {

struct ParameterCombination {
  double alpha;
  double lambda;
  int max_epochs;

  /* Used to construct the result identifier */
  friend std::ostream& operator<<(std::ostream& os,
                                  const ParameterCombination& pc);

  /* Write a values separated by |separator|. No line ending. */
  void row(std::ostream& os, int separator = '\t') const;
  /* Write a header of values separated by |separator|. No line ending. */
  void header(std::ostream& os, std::string prefix = "",
              int separator = '\t') const;
};

struct InputParameters {
  using FileInformation = core::FileInformation;

  InputParameters(ParameterCombination alg, FileInformation network,
                  FileInformation predictive, FileInformation labeled)
      : alg(alg), network(network), predictive(predictive), labeled(labeled) {}

  std::string str() const;

  /* Used to construct the result identifier */
  friend std::ostream& operator<<(std::ostream& os, const InputParameters& ip);

  std::string simulation_name(const std::string& prefix) const;
  std::string classification_name(const std::string& prefix) const;

  bool validate() const;

  /* Write a values separated by |separator|. No line ending. */
  void row(std::ostream& os, int separator = '\t') const;
  /* Write a header of values separated by |separator|. No line ending. */
  void header(std::ostream& os, int separator = '\t') const;

  ParameterCombination alg;
  FileInformation network, predictive, labeled;
};

class Specification : public core::Specification {
 public:
  using FileList = std::vector<::core::FileInformation>;

  Specification();
  Specification(std::istream& json);
  virtual ~Specification();

  const std::vector<InputParameters>& inputs() const { return inputs_; }
  const std::vector<int>& neighborhood_orders() const { return nei_orders_; }

 private:
  virtual void ProcessTree();

  void PopulateInputList();

  void ReadNumericFileCombinations(const std::string& node_path,
                                   FileList& result);

  FileList networks_;
  FileList predictive_;
  FileList labeled_;
  std::vector<ParameterCombination> parameters_;
  std::vector<InputParameters> inputs_;
  std::vector<int> nei_orders_;
};

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_SPECIFICATION_H_
