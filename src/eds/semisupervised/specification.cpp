#include "./specification.h"

#include <cassert>

#include <array>
#include <istream>
#include <string>

#include <boost/lexical_cast.hpp>

#include "core/combinations.h"
#include "core/debug.h"
#include "core/filesystem.h"
#include "core/utility.h"

namespace eds {

namespace ssl {

std::ostream& operator<<(std::ostream& os, const ParameterCombination& pc) {
  os << "alpha=" << pc.alpha << ",lambda=" << pc.lambda
     << ",max_epochs=" << pc.max_epochs;
  return os;
}

void ParameterCombination::row(std::ostream& os, int separator) const {
  os << alpha << static_cast<char>(separator) << lambda
     << static_cast<char>(separator) << max_epochs;
}

void ParameterCombination::header(std::ostream& os, std::string prefix,
                                  int separator) const {
  os << '"' << prefix << "alpha" << '"' << static_cast<char>(separator) << '"'
     << prefix << "lambda" << '"' << static_cast<char>(separator) << '"'
     << prefix << "max_epochs" << '"';
}

std::string InputParameters::str() const {
  std::stringstream ss;
  ss << network.str();
  ss << predictive.str();
  ss << labeled.str();
  return ss.str();
}

std::ostream& operator<<(std::ostream& os, const InputParameters& ip) {
  os << ip.network << "," << ip.predictive << "," << ip.labeled << "," << ip.alg
     << ".tab";
  return os;
}

std::string InputParameters::simulation_name(const std::string& prefix) const {
  std::stringstream name;
  name << prefix << *this;
  return name.str();
}

std::string InputParameters::classification_name(
    const std::string& prefix) const {
  std::stringstream name;
  name << prefix << "classification," << *this;
  return name.str();
}

bool InputParameters::validate() const {
  bool n = filesystem::exists(network.name);
  bool p = filesystem::exists(predictive.name);
  bool l = filesystem::exists(labeled.name);
  if (!n) printd(WARN, "File %s not found.\n", network.name.c_str());
  if (!p) printd(WARN, "File %s not found.\n", predictive.name.c_str());
  if (!l) printd(WARN, "File %s not found.\n", labeled.name.c_str());
  return n && p && l;
}

void InputParameters::row(std::ostream& os, int separator) const {
  network.row(os, separator);
  os << static_cast<char>(separator);
  predictive.row(os, separator);
  os << static_cast<char>(separator);
  labeled.row(os, separator);
  os << static_cast<char>(separator);
  alg.row(os, separator);
  os << static_cast<char>(separator) << '"' << simulation_name("") << '"';
  os << static_cast<char>(separator) << '"' << classification_name("") << '"';
}

void InputParameters::header(std::ostream& os, int separator) const {
  network.header(os, "network.", separator);
  os << static_cast<char>(separator);
  predictive.header(os, "predictive.", separator);
  os << static_cast<char>(separator);
  labeled.header(os, "labeled.", separator);
  os << static_cast<char>(separator);
  alg.header(os, "alg.", separator);
  os << static_cast<char>(separator) << "\"output.simulation_name\"";
  os << static_cast<char>(separator) << "\"output.classification_name\"";
}

Specification::Specification() : core::Specification() {}
Specification::Specification(std::istream& json) : core::Specification() {
  read(json);
}
Specification::~Specification() {}

void Specification::ProcessTree() {
  std::string problem = pt.get<std::string>("problem");
  if (problem != "semisupervised learning") {
    printd(ERROR,
           "Cannot parse specification for problem '%s'."
           " Expected problem 'semisupervised learning'.\n",
           problem.c_str());
  }
  ReadNumericFileCombinations("input.network", networks_);
  ReadNumericFileCombinations("input.predictive", predictive_);
  ReadNumericFileCombinations("input.labeled", labeled_);
  GetCombinations<std::string, double>(
      Node("algorithm.parameters"),
      [&](const auto& keys, const auto& iterators) {
        parameters_.emplace_back();
        auto& p = parameters_.back();
        auto kit = keys.begin();
        for (const auto& it : iterators) {
          const std::string key = *kit;
          // TODO : not checking whether all parameters were read.
          if (key == "alpha")
            p.alpha = *it;
          else if (key == "lambda")
            p.lambda = *it;
          else if (key == "max_epochs")
            p.max_epochs = static_cast<int>(*it);
          ++kit;
        }
      },
      [&](const std::string&) -> bool { return true; },
      [&](const std::string& value) -> double { return std::stod(value); });
  PopulateInputList();
  nei_orders_ = ReadList<int>(
      Node("classification.neighborhood order"),
      [](const auto& value) { return boost::lexical_cast<int>(value); });
}

void Specification::PopulateInputList() {
  using std::array;
  using std::string;

  core::FileInformationMatcher matcher;
  matcher.push(networks_);
  matcher.push(predictive_);
  matcher.push(labeled_);

  auto keys = ReadList<std::string>(Node("input.match files by"));
  for (const auto& key : keys) matcher.add_criterion(key);

  matcher.match([&](const auto& iterators) {
    for (const auto& param : parameters_)
      inputs_.emplace_back(param, *iterators[0], *iterators[1], *iterators[2]);
  });
}

void Specification::ReadNumericFileCombinations(const std::string& node_path,
                                                FileList& result) {
  std::string prefix = filesystem::ensure_trailing_separator(
      core::expand_environmental_vars(pt.get<std::string>("input.prefix")));
  FormatCombinations<std::string, double>(
      Node(node_path),
      [&result, &prefix](const auto& name, const auto& keys,
                         const auto& iterators) {
        result.emplace_back();
        auto& item = result.back();
        item.name = prefix + name;
        auto kit = keys.begin();
        for (const auto& it : iterators) {
          item.param[*kit] = *it;
          ++kit;
        }
      },
      [&](const std::string& value) -> double { return std::stod(value); });
  /* If there are no combinations, push the format itself there. */
  if (result.empty()) {
    result.emplace_back();
    auto& item = result.back();
    item.name = prefix + Node(node_path).get<std::string>("format");
  }
}

}  // namespace ssl

}  // namespace eds
