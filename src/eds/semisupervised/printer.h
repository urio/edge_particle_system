#ifndef EDS_SSL_PRINTER_H_
#define EDS_SSL_PRINTER_H_

#include "./eds.h"

#include "core/observer.h"

namespace eds {

namespace ssl {

/* Prints on terminal information about the current state of a system’s
 * dynamics. */
class Printer : public Observer {
 public:
  Printer(EdgeDynamicsSystem* subject) : Observer(subject) {}

  void update();

 private:
  void print_class(const struct ClassDynamics& state) const;
};

}  // namespace ssl

}  // namespace eds

#endif  // EDS_SSL_PRINTER_H_
