#include "./file_information.h"

#include <sstream>
#include <map>
#include <string>
#include <vector>
#include <iostream>

#include <boost/functional/hash.hpp>

#include "core/combinations.h"

namespace core {

std::string FileInformation::str(bool include_tree) const {
  std::stringstream ss;
  ss << name << "\n";
  if (include_tree) {
    for (const auto& node : param) {
      ss << "  " << node.first << ": " << node.second << "\n";
    }
  }
  return ss.str();
}

std::ostream& operator<<(std::ostream& os, const FileInformation& fi) {
  bool first = true;
  for (const auto& node : fi.param) {
    if (!first) os << ",";
    first = false;
    os << node.first << "=" << node.second;
  }
  return os;
}

bool FileInformation::operator==(const FileInformation& other) const {
  return name == other.name;
}

void FileInformation::row(std::ostream& os, int separator) const {
  const char kSep = static_cast<char>(separator);
  bool first = true;
  for (const auto& node : param) {
    if (!first) os << kSep;
    first = false;
    os << node.second;
  }
  os << kSep << '"' << name << '"';
}

void FileInformation::header(std::ostream& os, std::string prefix,
                             int separator) const {
  const char kSep = static_cast<char>(separator);
  bool first = true;
  for (const auto& node : param) {
    if (!first) os << kSep;
    first = false;
    os << '"' << prefix << node.first << '"';
  }
  os << kSep << '"' << prefix << "name" << '"';
}

std::size_t hash_value(const FileInformation& b) {
  boost::hash<std::string> hasher;
  return hasher(b.name);
}

FileInformationMatcher::FileInformationMatcher() {}

FileInformationMatcher::~FileInformationMatcher() {}

void FileInformationMatcher::push(const std::vector<FileInformation>& files) {
  lists_.push_back(files);
}

void FileInformationMatcher::add_criterion(const std::string& criterion) {
  criteria_.push_back(criterion);
}

void FileInformationMatcher::match(
    std::function<
        void(const std::vector<std::vector<FileInformation>::const_iterator>&)>
        match_files) {
  Combinations<decltype(lists_)> combinations{lists_};

  /* For each list, store whether the lists contains the criteria */
  std::vector<std::map<std::string, bool>> has_key;
  has_key.resize(lists_.size());
  std::size_t lst_idx = 0;
  for (const auto& lst : lists_) {
    auto& has_key_map = has_key[lst_idx];
    for (const auto& criterion : criteria_) {
      bool contains = lst[0].param.find(criterion) != lst[0].param.end();
      has_key_map[criterion] = contains;
    }
    ++lst_idx;
  }

  /* Run through all combinations checking for matches.
   * Lists without params are considered as match */
  for (const auto& iterators : combinations) {
    std::map<std::string, double> first_value;
    bool matched = true;
    /* To have a match, we cycle the iterators checking all criteria. */
    for (lst_idx = 0; lst_idx < lists_.size(); ++lst_idx) {
      for (const auto& criterion : criteria_) {
        /* If this list has this criterion, we will use it.
         * Otherwise, we assume it matches. */
        if (has_key[lst_idx].at(criterion)) {
          double value = iterators[lst_idx]->param.at(criterion);
          /* If it is the first list with this criterion, we initialize the
           * base value for matching. */
          if (first_value.find(criterion) == first_value.end()) {
            first_value[criterion] = value;
          }
          matched = matched && (first_value[criterion] == value);
          if (!matched) break;
        }
      }
    }
    if (matched) match_files(iterators);
  }
}

}  // namespace core
