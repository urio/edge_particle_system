#include "./benchmark.h"

#include <iomanip>
#include <stdexcept>
#include <string>
#include <sstream>

#include "./debug.h"

namespace core {

Benchmark::Snapshot::Snapshot() {
  int ret = gettimeofday(&real_time, NULL);
  if (ret != 0) {
    printd(WARN, "gettimeofday() has failed.\n");
  }
  cpu_time = clock();
  if (cpu_time == -1) {
    printd(WARN, "clock() has failed.\n");
  }
}

long double Benchmark::Snapshot::cpu_diff(const Snapshot& b) const {
  long double time = static_cast<long double>(cpu_time);
  time -= static_cast<long double>(b.cpu_time);
  return time / CLOCKS_PER_SEC;
}

long double Benchmark::Snapshot::real_diff(const Snapshot& b) const {
  long double a_sec = real_as_seconds();
  long double b_sec = b.real_as_seconds();
  return a_sec - b_sec;
}

long double Benchmark::Snapshot::real_as_seconds() const {
  long double sec = static_cast<long double>(real_time.tv_sec);
  sec += static_cast<long double>(real_time.tv_usec) / 1000000.0;
  return sec;
}

void Benchmark::push() { snapshots_.push_back(Snapshot()); }

void Benchmark::pop() { snapshots_.pop_back(); }

long double Benchmark::cpu(std::size_t from, std::size_t to) const {
  std::size_t len = snapshots_.size();
  if (from > len || to > len) {
    printd(ERROR,
           "There’s not enough snapshots to compare. "
           "(Trying to compare elapsed time between #%d and #%d snaphots.)\n",
           static_cast<int>(len) - static_cast<int>(from),
           static_cast<int>(len) - static_cast<int>(to));
    throw std::runtime_error("Invalid snapshot index");
  }
  const Snapshot& a = snapshots_.at(len - from - 1);
  const Snapshot& b = snapshots_.at(len - to - 1);
  return a.cpu_diff(b);
}

long double Benchmark::real(std::size_t from, std::size_t to) const {
  std::size_t len = snapshots_.size();
  if (from > len || to > len) {
    printd(ERROR,
           "There’s not enough snapshots to compare. "
           "(Trying to compare elapsed time between #%d and #%d snaphots.)\n",
           static_cast<int>(len) - static_cast<int>(from),
           static_cast<int>(len) - static_cast<int>(to));
    throw std::runtime_error("Invalid snapshot index");
  }
  const Snapshot& a = snapshots_.at(len - from - 1);
  const Snapshot& b = snapshots_.at(len - to - 1);
  return a.real_diff(b);
}

std::string Benchmark::str(std::size_t from, std::size_t to) const {
  std::stringstream ss;
  ss << std::setprecision(4) << std::fixed;
  ss << "CPU=" << cpu(from, to) << "s - ";
  ss << "Real=" << real(from, to) << "s";
  return ss.str();
}

}  // namespace core
