#ifndef CORE_SPECIFICATION_H_
#define CORE_SPECIFICATION_H_

#include <cstddef>

#include <functional>
#include <istream>
#include <vector>
#include <string>
#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/format.hpp>

#include "./output/policy.h"
#include "./combinations.h"

namespace core {

namespace spec {

template <typename T>
using filter_t = std::function<T(const std::string&)>;

static filter_t<bool> default_use_key = [](const std::string&) -> bool {
  return true;
};

template <typename T>
static filter_t<T> default_transform_key = [](const std::string& key) -> T {
  return static_cast<T>(key);
};

template <typename T>
static filter_t<T> default_transform_value = [](const std::string& key) -> T {
  return static_cast<T>(key);
};

}  // namespace spec

class Specification {
 public:
  using Tree = boost::property_tree::ptree;

  Specification();
  virtual ~Specification();

  void read(std::istream& json);

  std::string problem() const;
  std::string algorithm() const;
  std::string input_path() const;

  OutputPolicy& output();

  template <typename ValueType = std::string>
  ValueType get(std::string value) const {
    return pt.get<ValueType>(value);
  }

 protected:
  Tree Node(const std::string& node_path) const;

  /* Read all keys within "node" : { "key1": ..., "key2": ... }
   * Define |use_key| to filter out some keys. */
  template <typename T = std::string>
  std::vector<T> ReadKeys(
      const Tree& node, spec::filter_t<bool> use_key = spec::default_use_key,
      spec::filter_t<T> transform_key = spec::default_transform_key<T>) {
    std::vector<T> result;
    result.reserve(node.size());  // Maximum size
    for (const auto& subnode : node) {
      if (use_key(subnode.first)) {
        result.push_back(transform_key(subnode.first));
      }
    }
    return result;
  }

  /* Read list of a |node|.
   * If "value": [0 1 2 3], a vector with items 0, 1, 2, and 3 is returned.
   * However, if the data is not contained in brackets, "value": [...], it is
   * not a list and we consider it to be a self-link from the root level.
   * Example:
   * {
   *  "first" : {
   *    "a" : [4],
   *    "second": {
   *      "a" : [0 1],
   *      "b" : "first.a"
   *    }
   *  }
   *  ReadList(Node("first.a")) returns vector{4}
   *  ReadList(Node("first.second.a")) returns vector{0, 1}
   *  ReadList(Node("first.second.b")) returns vector{4}
   *
   *  Note that value of "first.second.b" is a path from the root and not from
   *  the current level.
   *
   *  ReadList will recursively follow links until find a "linked": [...].
   * */
  template <typename T = std::string>
  std::vector<T> ReadList(
      const Tree& node,
      spec::filter_t<T> transform_value = spec::default_transform_value<T>) {
    if (node.size() == 0) {
      /* Node is not a list. We follow it as a link until we find a list. */
      return ReadList<T>(Node(node.data()), transform_value);
    }
    std::vector<T> result;
    result.reserve(node.size());
    for (const auto& item : node) {  // Read properties within child node
      const auto& value = item.second.get_value<std::string>();
      result.push_back(transform_value(value));
    }
    return result;
  }

  /* Read entries in { "key1": [...], "key2": [...] } and put each list in a
   * vector.
   * Define |use_key| to determine which keys should be ignored. */
  template <typename T = std::string>
  std::vector<std::vector<T>> ReadLists(
      const Tree& root, spec::filter_t<bool> use_key = spec::default_use_key,
      spec::filter_t<T> transform_value = spec::default_transform_value<T>) {
    std::vector<std::vector<T>> result;
    for (const auto& node : root) {
      if (!use_key(node.first)) continue;
      result.push_back(ReadList<T>(node.second, transform_value));
    }
    return result;
  }

  /* For "root": { "key1": [...], "key2": [...] }, construct all combinations
   * of values of "key1", "key2" etc.
   * |use_key| callback: filter which keys should be used on combinations.
   * |combination| callback:
   * [](const auto& keys, const auto& iterators) {
   *   // keys[i] has the value *iterators[i]
   * }
   * */
  template <typename KeyT = std::string, typename ValueT = std::string>
  void GetCombinations(
      const Tree& root,
      std::function<void(
          const std::vector<KeyT>&,
          const std::vector<typename std::vector<ValueT>::const_iterator>&)>
          combination,
      spec::filter_t<bool> use_key = spec::default_use_key,
      spec::filter_t<ValueT> transform_value =
          spec::default_transform_value<ValueT>) {
    auto vectors = ReadLists<ValueT>(root, use_key, transform_value);
    auto keys = ReadKeys<KeyT>(root, use_key);

    Combinations<decltype(vectors)> combinations(vectors);
    for (const auto& c : combinations) combination(keys, c);
  }

  /* For
   * "root": {
   *    "type": "...",
   *    "format": "param1=%1% param2=%2%",
   *    "p1": [0 1],
   *    "p2": [2 3]
   * }
   * Values of "p1" and "p2" are replaced IN ORDER on "format" string.
   * Ignoring "type" and "format" keys, the function assumes all other
   * key-value pairs have lists of values.
   * For the above example, the result will have the following values:
   *   param1=0 param2=2
   *   param1=0 param2=3
   *   param1=1 param2=2
   *   param1=1 param2=3
   * Each combination has its own property tree which allows to recover the
   * values used on generating the string.
   * Example with above specification:
   *   list = FormatCombinations(Node("root"));
   *   list[0].value == "param1=0 param2=2"
   *   list[0].get<int>("p1") == 0
   */
  template <typename KeyT = std::string, typename T = std::string>
  void FormatCombinations(
      const Tree& node,
      std::function<
          void(const std::string&, const std::vector<KeyT>&,
               const std::vector<typename std::vector<T>::const_iterator>&)>
          combination,
      std::function<T(const std::string&)> transform_value =
          [](const auto& key) -> T { return key; }) {
    const std::string format_str = node.get<std::string>("format");
    boost::format format(format_str);
    try {
      GetCombinations<KeyT, T>(
          node.get_child("args"), [&](const auto& keys, const auto& iterators) {
            for (std::size_t k = 0; k < iterators.size(); ++k) {
              format % *iterators[k];
            }
            combination(boost::str(format), keys, iterators);
          }, spec::default_use_key, transform_value);
    } catch (boost::property_tree::ptree_bad_path e) {
    }
  }

  Tree pt;

 private:
  virtual void ProcessTree() = 0;

  std::unique_ptr<OutputPolicy> output_;
};

}  // namespace core

#endif  // CORE_SPECIFICATION_H_
