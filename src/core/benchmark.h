#ifndef BENCHMARK_H_
#define BENCHMARK_H_

#include <sys/time.h>
#include <cstddef>
#include <ctime>

#include <deque>
#include <string>

namespace core {

/* This class has a stack of time snapshots.
 * Using push() and pop() use can manage to have many snapshots and get
 * a diff between them. */
class Benchmark {
 private:
  struct Snapshot {
   public:
    Snapshot();
    ~Snapshot() = default;

    long double cpu_diff(const Snapshot& b) const;
    long double real_diff(const Snapshot& b) const;

   protected:
    long double real_as_seconds() const;

   private:
    struct timeval real_time;
    clock_t cpu_time;
  };

 public:
  Benchmark() = default;
  ~Benchmark() = default;

  /* Place in the stack the a new time snapshot. */
  void push();
  /* Pop from stack the most recent time snapshot. */
  void pop();

  /* Get the diff between the |from|-th most recent time snapshot and
   * the |to|-th most current time snapshot.
   * Default values diffs between stack’s top and top - 1 snapshots. */
  long double cpu(std::size_t from = 0, std::size_t to = 1) const;
  long double real(std::size_t from = 0, std::size_t to = 1) const;

  std::string str(std::size_t from = 0, std::size_t to = 1) const;

 private:
  std::deque<Snapshot> snapshots_;
};

}  // namespace core

#endif
