/* Copyright © 2013 Paulo Roberto Urio */
#ifndef DEBUG_H_
#define DEBUG_H_

#include <cstdio>
#include <cstring>

/* Debugging levels */
#define TESTING 0
#define INFO 10
#define WARN 20
#define ERROR 30
#define SUPPRESS_ALL 40

/* Controls the verbosity of DEBUG macro */
#ifndef DEBUG_VERBOSITY
#ifndef NDEBUG /* Debug mode */
#define DEBUG_VERBOSITY TESTING
#else /* Release mode */
#define DEBUG_VERBOSITY INFO
#endif
#endif

/* Print file:line:function() before each message */
#ifndef DEBUG_LOCATION
#define DEBUG_LOCATION 1
#endif

/* Controls whether debug() should print colorful messages or not */
#ifndef COLORED_DEBUG
#define COLORED_DEBUG 1
#endif

#define TXTBLK "\033[0;30m"  // Black - Regular
#define TXTRED "\033[0;31m"  // Red
#define TXTGRN "\033[0;32m"  // Green
#define TXTYLW "\033[0;33m"  // Yellow
#define TXTBLU "\033[0;34m"  // Blue
#define TXTPUR "\033[0;35m"  // Purple
#define TXTCYN "\033[0;36m"  // Cyan
#define TXTWHT "\033[0;37m"  // White
#define BLDBLK "\033[1;30m"  // Black - Bold
#define BLDRED "\033[1;31m"  // Red
#define BLDGRN "\033[1;32m"  // Green
#define BLDYLW "\033[1;33m"  // Yellow
#define BLDBLU "\033[1;34m"  // Blue
#define BLDPUR "\033[1;35m"  // Purple
#define BLDCYN "\033[1;36m"  // Cyan
#define BLDWHT "\033[1;37m"  // White
#define UNKBLK "\033[4;30m"  // Black - Underline
#define UNDRED "\033[4;31m"  // Red
#define UNDGRN "\033[4;32m"  // Green
#define UNDYLW "\033[4;33m"  // Yellow
#define UNDBLU "\033[4;34m"  // Blue
#define UNDPUR "\033[4;35m"  // Purple
#define UNDCYN "\033[4;36m"  // Cyan
#define UNDWHT "\033[4;37m"  // White
#define BAKBLK "\033[40m"    // Black - Background
#define BAKRED "\033[41m"    // Red
#define BAKGRN "\033[42m"    // Green
#define BAKYLW "\033[43m"    // Yellow
#define BAKBLU "\033[44m"    // Blue
#define BAKPUR "\033[45m"    // Purple
#define BAKCYN "\033[46m"    // Cyan
#define BAKWHT "\033[47m"    // White
#define TXTRST "\033[0m"     // Text Reset

#if COLORED_DEBUG == 1
#define __debug_msg_end_tag "\033[0m"
#define __debug_msg_color_TESTING "\033[1m"
#define __debug_msg_color_INFO "\033[1;34m"
#define __debug_msg_color_WARN "\033[1;33m"
#define __debug_msg_color_ERROR "\033[1;31m"
#else
#define __debug_msg_end_tag
#define __debug_msg_color_TESTING
#define __debug_msg_color_INFO
#define __debug_msg_color_WARN
#define __debug_msg_color_ERROR
#endif

#define __debug_msg_TESTING "Debug: "
#define __debug_msg_INFO "Info: "
#define __debug_msg_WARN "Warn: "
#define __debug_msg_ERROR "Error: "

#include <boost/thread/thread.hpp>

#define __SHORT_FORM_OF_FILE__ \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define __print_debug(level, preamble, fmt, ...)                      \
  if (level >= DEBUG_VERBOSITY) {                                     \
    if (DEBUG_LOCATION) {                                             \
      std::stringstream id;                                           \
      id << boost::this_thread::get_id();                             \
      fprintf(stderr, "\n[%s] %s:%d:%s(): ", id.str().c_str(),          \
              __SHORT_FORM_OF_FILE__, __LINE__, __FUNCTION__);        \
    }                                                                 \
    fprintf(stderr, preamble fmt __debug_msg_end_tag, ##__VA_ARGS__); \
    fflush(stderr);                                                   \
  }

/*
 * Print a message respecting the verbosity level set during compiling
 * This macro should be use to print messages that doesn't fit as
 * program output data, but for information, warning and errors.
 */
#define printd(level, ...)                                              \
  do {                                                                  \
    __print_debug(level, __debug_msg_color_##level __debug_msg_##level, \
                  ##__VA_ARGS__);                                       \
  } while (0)

/* Debug macro.  Only generate code inside DEBUG() if not in release mode */
#ifndef NDEBUG
#define DEBUG(x) x
#else
#define DEBUG(x)
#endif

#endif  // DEBUG_H_
