#ifndef GPU_DEVICES
#define GPU_DEVICES

namespace gpu {

class Devices {
 public:
  Devices();
  ~Devices();

  /* Properties of a device
   * Check cuda documentation for more details */
  struct cudaDeviceProp getProperties(int device);

  /* Initialize device */
  void init(int device);

  /* Available CUDA-enabled devices */
  int count() const;

  /* Set current device */
  void setDevice(int device);

  /* Current selected device id */
  int getCurrentDevice();

  /* Destroys and cleans all resources */
  void Reset();
};

}  // namespace gpu

#endif
