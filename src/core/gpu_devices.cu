#include "./gpu_devices.h"

#include <stdexcept>

#include <cuda.h>

#include "./debug.h"
#include "./cutil.cuh"

namespace gpu {

Devices::Devices() {}
Devices::~Devices() {
  Reset();
}

/* Properties of a device
 * Check cuda documentation for more details */
struct cudaDeviceProp Devices::getProperties(int device) {
  struct cudaDeviceProp properties;
  cudaSafeCall(cudaGetDeviceProperties(&properties, device));
  return properties;
}

void Devices::init(int device) {
  int device_count = count();

  if (device_count == 0) {
    printd(ERROR, "No devices supporting CUDA.\n");
    throw std::runtime_error("Not devices supporting CUDA");
  }

  if (device < 0) {
    printd(ERROR, "Invalid device id %d.\n", device);
    throw std::runtime_error("Invalid device id");
  }

  if (device >= device_count) {
    printd(ERROR, "Invalid device id %d (max = %d).\n", device, device_count);
    throw std::runtime_error("Invalid device id");
  }

  cudaDeviceProp deviceProp = getProperties(device);

  if (deviceProp.computeMode == cudaComputeModeProhibited) {
    printd(ERROR,
           "Error: device is running in <Compute Mode Prohibited>, no threads "
           "can use ::cudaSetDevice().\n");
    throw std::runtime_error("Cannot used cudaSetDevice");
  }

  if (deviceProp.major < 1) {
    printd(ERROR, "gpuDeviceInit(): GPU device does not support CUDA.\n");
    throw std::runtime_error("GPU device does not support CUDA");
  }

  cudaSafeCall(cudaSetDevice(device));
  printd(INFO, "Init CUDA Device [%d]: \"%s\"\n", device, deviceProp.name);
}

/* Available CUDA-enabled devices */
int Devices::count() const {
  int count = 0;
  cudaSafeCall(cudaGetDeviceCount(&count));
  return count;
}

/* Set current device */
void Devices::setDevice(int device) {
  printd(TESTING, "Setting GPU device %d.\n", device);
  cudaSafeCall(cudaSetDevice(device));
}

/* Current selected device id */
int Devices::getCurrentDevice() {
  int current;
  cudaSafeCall(cudaGetDevice(&current));
  return current;
}

/* Destroys and cleans all resources */
void Devices::Reset() { cudaSafeCall(cudaDeviceReset()); }

}  // namespace gpu
