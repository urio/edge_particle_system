#ifndef CATEGORICAL_H_
#define CATEGORICAL_H_

#include <cstddef>

#include <armadillo>

#include <iterator>
#include <set>
#include <sstream>
#include <string>
#include <iostream>

#include <boost/current_function.hpp>
#include <jules/dataframe/column.hpp>

#include "./debug.h"

namespace core {

/* Takes a column of values and treats its contents as categorical values.
 * A categorical column has many levels, which are all enumerated. */
template <typename T>
class Categorical {
 public:
  Categorical() = delete;

  ~Categorical() {}

  Categorical(const jules::column& column)
      : column_(jules::coerce_to<T>(column)),
        column_view_(column_.template view<T>()) {
    for (const auto& val : column_view_) categories_.insert(val);
  }

  // Categorical(const arma::colvec& column)
  //     : column_(T{}, column.size()),
  //       column_view_(column_.template view<T>()) {
  //   for (std::size_t i = 0; i < column.size(); ++i) 
  //     column_view_[i] = column[i];
  //   for (const auto& val : column_view_) categories_.insert(val);
  // }

  // Get the enumerated value of |category|.
  std::size_t enumerate(const T& category) const {
    auto pos = categories_.find(category);
    if (pos == categories_.end()) throw std::runtime_error("Invalid category");
    return std::distance(categories_.begin(), pos);
  }

  // Get the categorical enumeration at column’s |index|th position.
  std::size_t at(std::size_t index) const {
    if (index >= column_view_.size()) {
      printd(ERROR,
             "Column index out of range. "
             "(index = %zd, size = %zd)\n",
             index, column_view_.size());
      throw std::runtime_error("Column index out of range");
    }
    return enumerate(column_view_[index]);
  }

  std::size_t operator[](std::size_t index) const { return at(index); }

  std::size_t size() const { return categories_.size(); }

 private:
  std::set<T> categories_;
  jules::column column_;
  jules::column_view<T> column_view_;
};

}  // namespace core

#endif  // CATEGORICAL_H_
