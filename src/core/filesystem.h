#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <string>

namespace filesystem {

// Extract the path of |filename|.
std::string extract_path(const std::string &filename);

// Add the path separator if needed
void ensure_trailing_separator(std::string &path);
std::string ensure_trailing_separator(const std::string &path);

// Check whether |filename| exists or not.
bool exists(const std::string &filename);

// Delete |filename|.
bool remove(const std::string &filename);

// Ensure the path of |filename| exists.
bool ensure_path(const std::string &filename);

}  // namespace filesystem

#endif  // FILESYSTEM_H_
