#include "./edge_vector.h"

namespace igraph {

void EdgeVector::BuildVector(const Graph& graph) {
  int ecount = graph.ecount();

  clear();
  reserve(ecount);
  for (int eid = 0; eid < ecount; ++eid) {
    emplace_back(graph.edge(eid));
  }
}

}  // namespace igraph
