#ifndef EDGE_VECTOR_H
#define EDGE_VECTOR_H

#include <cstddef>

#include <utility>
#include <vector>

#include <igraphpp/igraph.hpp>

namespace igraph {

class EdgeVector : public std::vector<std::pair<std::size_t, std::size_t>> {
 public:
  EdgeVector() = default;

  ~EdgeVector() = default;

  EdgeVector(const Graph& graph) { BuildVector(graph); }

  void BuildVector(const igraph::Graph& graph);
};

}  // namespace igraph

#endif  // EDGE_VECTOR_H
