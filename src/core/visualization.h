
#include <thread>

#include <jules/dataframe/dataframe.hpp>
#include <igraphpp/igraph.hpp>

#include <vtkLookupTable.h>
#include <vtkDoubleArray.h>
#include <vtkGraphLayoutView.h>
#include <vtkIntArray.h>
#include <vtkInteractorStyleRubberBand3D.h>
#include <vtkMutableUndirectedGraph.h>
#include <vtkPoints.h>
#include <vtkRenderedGraphRepresentation.h>
#include <vtkSmartPointer.h>
#include <vtkViewTheme.h>

namespace core {

class ColorTable {
 public:
  using dataframe = jules::dataframe;

  ColorTable() = default;
  ~ColorTable() = default;

  void SetColors(const dataframe& colors);
  void PopulateDefaultColors();

  const vtkSmartPointer<vtkLookupTable>& table() const { return table_; }

 private:
  vtkSmartPointer<vtkLookupTable> table_;
};

class CustomRepresentation : public vtkRenderedGraphRepresentation {
 public:
  static CustomRepresentation* New();
  vtkTypeMacro(CustomRepresentation, vtkRenderedGraphRepresentation);

  void SetVertexSize(int vertexSize);
  void SetEdgeWidth(int factor);
};

class KeyPressInteractorStyle : public vtkInteractorStyleRubberBand3D {
 public:
  static KeyPressInteractorStyle* New();
  vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleRubberBand3D);

  virtual void OnKeyPress();

  void set_representation(vtkRenderedGraphRepresentation* representation) {
    representation_ = representation;
  }
  const vtkRenderedGraphRepresentation* representation() const {
    return representation_;
  }

 private:
  vtkRenderedGraphRepresentation* representation_ = nullptr;
};

class Visualization {
 public:
  using Graph = igraph::Graph;
  using dataframe = jules::dataframe;
  using column = jules::column;

  void SetGraph(const Graph& graph);
  void SetCoordinates(const dataframe& coord);
  void SetEdgeColors(const column& colors);
  void SetVertexColors(const column& colors);
  void SetVertexLabels(const column& labels);
  void SetEdgeLabels(const column& labels);

  void Initialize();
  void Render();
  void Start();

 private:
  void SetUpDefaultVertexLabels();

  vtkSmartPointer<vtkMutableUndirectedGraph> graph_;
  vtkSmartPointer<vtkPoints> pos_;
  vtkSmartPointer<vtkViewTheme> theme_;
  vtkSmartPointer<vtkIntArray> vcolors_;
  vtkSmartPointer<vtkIntArray> ecolors_;
  vtkSmartPointer<vtkIntArray> vlabels_;
  vtkSmartPointer<vtkDoubleArray> elabels_;
  vtkSmartPointer<vtkGraphLayoutView> layout_;
  vtkSmartPointer<CustomRepresentation> representation_;
  vtkSmartPointer<KeyPressInteractorStyle> style_;
};

}  // namespace core
