#ifndef CORE_RANDOM_H_
#define CORE_RANDOM_H_

#include <random>

namespace core {

extern std::mt19937 global_random;

} // namespace core

#endif // ifndef CORE_RANDOM_H_
