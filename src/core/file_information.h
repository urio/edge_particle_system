#ifndef CORE_FILE_INFORMATION_H_
#define CORE_FILE_INFORMATION_H_

#include <cstddef>

#include <functional>
#include <string>
#include <map>
#include <ostream>
#include <vector>

namespace core {

struct FileInformation {
  FileInformation() : name(""), param() {}

  FileInformation(const FileInformation& other)
      : name(other.name), param(other.param) {}

  std::string name;
  std::map<std::string, double> param;

  std::string str(bool include_tree = true) const;

  friend std::ostream& operator<<(std::ostream& os, const FileInformation& fi);

  /* Write a values separated by |separator|. No line ending. */
  void row(std::ostream& os, int separator = '\t') const;
  /* Write a header of values separated by |separator|. No line ending. */
  void header(std::ostream& os, std::string prefix = "",
              int separator = '\t') const;

  bool operator==(const FileInformation& other) const;
};

extern std::size_t hash_value(const FileInformation& b);

/* Matches many lists of FileInformation according to given criterion.
 * A criterion is a key in "FileInformation::param" map. */
class FileInformationMatcher {
 public:
  FileInformationMatcher();
  virtual ~FileInformationMatcher();

  /* Add a list of |files|.
   * If you push() two lists, the matches are between files of those to lists.
   */
  void push(const std::vector<FileInformation>& files);

  /* Add new matching criterion.
   * If no criteria is added, |match| returns all possible possible
   * combinations of the file lists. */
  void add_criterion(const std::string& criterion);

  /* Get all matches accordingo the the criteria.
   * First, we assume each vector of files has the same |param|.
   * That is, different lists can have different params, but files in a single
   * list _must_ contain all the same param map.
   * If a file does not have a criterion parameter, then it is assumed to be a
   * match.
   * Examples.
   *
   * FileInformation a, b, c;
   * a.param["alpha"] = 1.0;
   * a.param["beta"] = 0.0;
   *
   * b.param["alpha"] = 1.2;
   * b.param["beta"] = 0.0;
   *
   * c.param["alpha"] = 1.0;
   *
   * |a| and |b| may be in the same file list:
   * vector<> list1{{a, b}};
   *
   * |c|, in this case, should be in a list alone:
   * vector<> list2{{c}};
   *
   * You can add those two lists for matching:
   * push(list1);
   * push(list2);
   *
   * You may want to select which parameters should match (none, one or more):
   * add_criterion("alpha");
   *
   * if you |add_criterion()| that does not exists, they match with existent
   * or all files.
   * add_criterion("beta") will match (|a|, |c|) and (|b|, |c|);
   * add_criterion("gamma") will match all possible combinations.
   *
   * To obtain all matches according to the given criteria:
   * match([](const auto& iterators) {
   *  // iterator[0] points to list1
   *  // iterator[1] points to list1
   * });
   */
  void match(
      std::function<void(
          const std::vector<std::vector<FileInformation>::const_iterator>&)>
          match_files);

 private:
  /* We assume each list has the same list of parameters so we can optimized a
   * bit */
  std::vector<std::vector<FileInformation>> lists_;
  std::vector<std::string> criteria_;
};

}  // namespace core

#endif  // ifndef CORE_FILE_INFORMATION_H_
