#include "./visualization.h"

#include <cstddef>

#include <iostream>
#include <stdexcept>
#include <string>

#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkDataSetAttributes.h>
#include <vtkGraphLayoutView.h>
#include <vtkGraphToGlyphs.h>
#include <vtkGraphLayoutView.h>
#include <vtkObjectFactory.h>
#include <vtkProperty.h>
#include <vtkRenderedGraphRepresentation.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>

#include "./debug.h"

namespace core {

vtkStandardNewMacro(CustomRepresentation);

void CustomRepresentation::SetVertexSize(int vertexSize) {
  this->VertexGlyph->SetScreenSize(vertexSize);
  this->VertexGlyph->Modified();
}

void CustomRepresentation::SetEdgeWidth(int factor) {
  this->EdgeActor->GetProperty()->SetLineWidth(factor);
  this->EdgeActor->GetProperty()->SetInterpolationToFlat();
  this->EdgeActor->Modified();
}

void KeyPressInteractorStyle::OnKeyPress() {
  vtkRenderWindowInteractor* rwi = this->Interactor;
  std::string key = rwi->GetKeySym();

  // Output the key that was pressed
  // std::cout << "Pressed " << key << std::endl;

  if (key == "w") {
    if (representation_ != nullptr) {
      representation_->SetEdgeLabelVisibility(
          !representation_->GetEdgeLabelVisibility());
      rwi->Render();
    }
  }

  if (key == "l") {
    if (representation_ != nullptr) {
      representation_->SetVertexLabelVisibility(
          !representation_->GetVertexLabelVisibility());
      rwi->Render();
    }
  }

  // Forward events
  vtkInteractorStyleRubberBand3D::OnKeyPress();
}

vtkStandardNewMacro(KeyPressInteractorStyle);

void ColorTable::SetColors(const dataframe& colors) {
  using jules::coerce_to;

  if (colors.ncol() != 4)
    throw std::runtime_error("Color table must have 4 columns");
  auto idx = coerce_to<double>(colors.select(0));
  auto r = coerce_to<double>(colors.select(1));
  auto g = coerce_to<double>(colors.select(2));
  auto b = coerce_to<double>(colors.select(3));
  auto vidx = idx.view<double>();
  auto vr = r.view<double>();
  auto vg = g.view<double>();
  auto vb = b.view<double>();
  table_ = vtkSmartPointer<vtkLookupTable>::New();
  table_->SetNanColor(1.0, 0.5, 0.0, 1.0);
  table_->SetNumberOfTableValues(colors.nrow());
  for (std::size_t i = 0; i < colors.nrow(); ++i) {
    table_->SetAnnotation(vtkVariant(vidx[i]),
                          vtkStdString(std::to_string(vidx[i])));
  }
  table_->SetIndexedLookup(1);
  table_->Build();
  for (std::size_t i = 0; i < colors.nrow(); ++i) {
    table_->SetTableValue(static_cast<vtkIdType>(vidx[i]), vr[i], vg[i], vb[i]);
    printd(TESTING, "Color %lld: RGB(%.1lf, %.1lf, %.1lf).\n",
           static_cast<vtkIdType>(vidx[i]), vr[i], vg[i], vb[i]);
  }
}

void ColorTable::PopulateDefaultColors() {
  using jules::column;
  column idx{{0, 1, 2, 3, 4}};
  column red{{1, 1, 0, 0, 1}};
  column grn{{1, 0, 0, 1, 1}};
  column blu{{1, 0, 1, 0, 0}};
  dataframe df;
  df.colbind(idx).colbind(red).colbind(grn).colbind(blu);
  SetColors(df);
}

void Visualization::SetGraph(const Graph& graph) {
  graph_ = vtkSmartPointer<vtkMutableUndirectedGraph>::New();
  graph_->SetNumberOfVertices(graph.vcount());
  int ecount = graph.ecount();
  bool edge_idx_warn = false;
  for (int e = 0; e < ecount; ++e) {
    igraph::Edge edge = graph.edge(e);
    graph_->AddEdge(edge.first, edge.second);
    /* Check index consistency between vtk and igraph */
    if (!edge_idx_warn) {
      int e_igraph = graph.eid(edge.first, edge.second);
      int e_vtk = static_cast<int>(graph_->GetEdgeId(edge.first, edge.second));
      if (e_igraph != e_vtk) {
        printd(WARN,
               "Visualization of edge colors will probably be wrong."
               " Indices between igraph and vtk graphs do not match."
               " (igraph edge = %d, vtk edge = %d).\n",
               e_igraph, e_vtk);
        edge_idx_warn = true;
      }
    }
  }
  vcolors_ = vtkSmartPointer<vtkIntArray>::New();
  ecolors_ = vtkSmartPointer<vtkIntArray>::New();
  vcolors_->SetNumberOfComponents(1);
  vcolors_->SetName("Color");
  if (vcolors_->Resize(graph.vcount()) == 0)
    throw std::runtime_error("Failed to resize vcolors");
  ecolors_->SetNumberOfComponents(1);
  ecolors_->SetName("Color");
  if (ecolors_->Resize(graph.ecount()) == 0)
    throw std::runtime_error("Failed to resize ecolors");
  SetUpDefaultVertexLabels();
}

void Visualization::SetCoordinates(const dataframe& coord) {
  using jules::column;

  if (coord.ncol() < 1)
    throw std::runtime_error("Needs at least one dimension of coordinates");
  if (coord.ncol() > 3)
    throw std::runtime_error("Allowed at most three dimensions of coordinates");
  std::size_t rows = coord.nrow();
  if (rows != static_cast<std::size_t>(graph_->GetNumberOfVertices()))
    throw std::runtime_error("Number of coordinates and vertices differ");
  auto x = coord.select(0);
  auto y = coord.ncol() >= 2 ? coord.select(1) : column(0, rows);
  auto z = coord.ncol() >= 3 ? coord.select(2) : column(0, rows);
  auto vx = x.coerce_to<double>().view<double>();
  auto vy = y.coerce_to<double>().view<double>();
  auto vz = z.coerce_to<double>().view<double>();
  pos_ = vtkSmartPointer<vtkPoints>::New();
  for (std::size_t i = 0; i < rows; ++i) {
    pos_->InsertNextPoint(vx[i], vy[i], vz[i]);
    printd(TESTING, "Coordinate of vertex %zd: %.1lf %.1lf %.1lf\n", i, vx[i],
           vy[i], vz[i]);
  }
}

void Visualization::SetEdgeColors(const column& colors) {
  if (colors.size() != static_cast<std::size_t>(ecolors_->GetSize()))
    throw std::runtime_error("Edge color array must be (E, 1)");
  auto col = jules::coerce_to<double>(colors);
  auto ecol = col.view<double>();

  ecolors_ = vtkSmartPointer<vtkIntArray>::New();
  ecolors_->SetNumberOfComponents(1);
  ecolors_->SetName("Color");
  ecolors_->SetNumberOfValues(graph_->GetNumberOfEdges());

  for (std::size_t i = 0; i < colors.size(); ++i) {
    ecolors_->SetValue(static_cast<vtkIdType>(i), static_cast<int>(ecol[i]));
  }
  graph_->GetEdgeData()->AddArray(ecolors_);
}

void Visualization::SetVertexColors(const column& colors) {
  if (colors.size() != static_cast<std::size_t>(vcolors_->GetSize()))
    throw std::runtime_error("Vertex color array must be (V, 1)");
  auto col = jules::coerce_to<double>(colors);
  auto vcol = col.view<double>();

  vcolors_ = vtkSmartPointer<vtkIntArray>::New();
  vcolors_->SetNumberOfComponents(1);
  vcolors_->SetName("Color");
  vcolors_->SetNumberOfValues(graph_->GetNumberOfVertices());

  for (std::size_t i = 0; i < colors.size(); ++i) {
    vcolors_->SetValue(static_cast<vtkIdType>(i), static_cast<int>(vcol[i]));
  }
  graph_->GetVertexData()->AddArray(vcolors_);
}

void Visualization::SetVertexLabels(const column& labels) {
  if (labels.size() != static_cast<std::size_t>(graph_->GetNumberOfVertices()))
    throw std::runtime_error("Label array must be (V, 1)");
  vlabels_ = vtkSmartPointer<vtkIntArray>::New();
  vlabels_->SetNumberOfComponents(1);
  vlabels_->SetName("labels");
  vlabels_->SetNumberOfValues(graph_->GetNumberOfVertices());
  auto lab = jules::coerce_to<double>(labels);
  auto vlab = lab.view<double>();
  for (std::size_t i = 0; i < labels.size(); ++i) {
    vlabels_->SetValue(static_cast<vtkIdType>(i), static_cast<int>(vlab[i]));
  }
  graph_->GetVertexData()->AddArray(vlabels_);
}

void Visualization::SetEdgeLabels(const column& labels) {
  if (labels.size() != static_cast<std::size_t>(graph_->GetNumberOfEdges()))
    throw std::runtime_error("Label array must be (E, 1)");
  elabels_ = vtkSmartPointer<vtkDoubleArray>::New();
  elabels_->SetNumberOfComponents(1);
  elabels_->SetName("labels");
  elabels_->SetNumberOfValues(graph_->GetNumberOfEdges());
  auto lab = jules::coerce_to<double>(labels);
  auto vlab = lab.view<double>();
  for (std::size_t i = 0; i < labels.size(); ++i) {
    elabels_->SetValue(static_cast<vtkIdType>(i), static_cast<double>(vlab[i]));
  }
  graph_->GetEdgeData()->AddArray(elabels_);
}

void Visualization::Initialize() {
  layout_ = vtkSmartPointer<vtkGraphLayoutView>::New();
  if (pos_.Get() != nullptr) graph_->SetPoints(pos_);

  theme_ = vtkSmartPointer<vtkViewTheme>::New();
  ColorTable table;
  table.PopulateDefaultColors();
  theme_->SetPointLookupTable(table.table());
  theme_->SetCellLookupTable(table.table());
  theme_->SetScalePointLookupTable(false);
  theme_->SetScaleCellLookupTable(false);

  representation_ = vtkSmartPointer<CustomRepresentation>::New();
  representation_->SetInputData(graph_);
  representation_->SetColorVerticesByArray(true);
  representation_->SetColorEdgesByArray(true);
  representation_->ApplyViewTheme(theme_);
  representation_->SetVertexSize(10);
  representation_->SetEdgeWidth(2);
  representation_->SetGlyphType(vtkGraphToGlyphs::SPHERE);
  representation_->SetVertexColorArrayName("Color");
  representation_->SetEdgeColorArrayName("Color");
  if (pos_.Get() != nullptr) {
    representation_->SetLayoutStrategyToPassThrough();
    layout_->SetLayoutStrategy("Pass Through");
  } else {
    representation_->SetLayoutStrategyToForceDirected();
    layout_->SetLayoutStrategy("Force Directed");
  }
  if (vlabels_.Get() != nullptr) {
    representation_->SetVertexLabelVisibility(false);
    representation_->SetVertexLabelArrayName("labels");
  }
  if (elabels_.Get() != nullptr) {
    representation_->SetEdgeLabelVisibility(false);
    representation_->SetEdgeLabelArrayName("labels");
  }

  style_ = vtkSmartPointer<KeyPressInteractorStyle>::New();
  style_->set_representation(representation_);

  layout_->AddRepresentation(representation_);
  // layout_->GetInteractor()->GetRenderWindow()->SetMultiSamples(8);
  // layout_->GetInteractor()->GetRenderWindow()->SetAAFrames(4);
  layout_->GetInteractor()->GetRenderWindow()->SetSize(800, 800);
  layout_->SetInteractionModeTo3D();
  layout_->GetInteractor()->SetInteractorStyle(style_);
  layout_->ResetCamera();
}
void Visualization::Render() { layout_->Render(); }
void Visualization::Start() {
  std::cout << "Visualization has started.\n";
  layout_->GetInteractor()->Start();
}

void Visualization::SetUpDefaultVertexLabels() {
  using jules::column;
  using std::size_t;

  size_t vcount = static_cast<size_t>(graph_->GetNumberOfVertices());
  auto lab = column{0.0, vcount};
  auto vlab = lab.view<double>();
  for (size_t i = 0; i < vcount; ++i) {
    vlab[i] = i;
  }
  SetVertexLabels(vlab);
}

}  // namespace core
