#ifndef UTILITY_H_
#define UTILITY_H_

#include <armadillo>

#include <fstream>
#include <stdexcept>
#include <string>

#include <jules/dataframe/dataframe.hpp>

#include "./debug.h"
#include "./random.h"

namespace jules {

template <typename T = std::string>
dataframe read_table(const std::string& filename) {
  std::ifstream file(filename);
  if (!file.is_open()) {
    printd(ERROR, "File not found: %s\n", filename.c_str());
    throw std::runtime_error("File not found");
  }
  dataframe_read_options opt;
  opt.header = false;
  dataframe df = dataframe::read(file, opt);
  df.coerce_to<T>();
  file.close();
  return df;
}

}  // namespace jules

namespace arma {

enum class TieMethod { Random, First, Last };

static inline arma::rowvec matrix_max_col_first(const arma::mat& matrix) {
  arma::uword col;
  arma::rowvec result(matrix.n_rows);
  for (arma::mat::size_type r = 0; r < matrix.n_rows; ++r) {
    matrix.row(r).max(col);
    result(r) = col;
  }
  return result;
}

static inline arma::rowvec matrix_max_col_last(const arma::mat& matrix) {
  arma::uword col;
  arma::rowvec result(matrix.n_rows);
  for (arma::mat::size_type r = 0; r < matrix.n_rows; ++r) {
    const auto& column = matrix.row(r);
    const auto max_val = column.max(col);
    const arma::uvec maxs = arma::find(column == max_val);
    result(r) = maxs[maxs.n_rows - 1];
  }
  return result;
}

template <typename RandomGenerator>
arma::rowvec matrix_max_col_random(const arma::mat& matrix,
                                   RandomGenerator rng) {
  arma::uword col;
  arma::rowvec result(matrix.n_rows);

  for (arma::mat::size_type r = 0; r < matrix.n_rows; ++r) {
    const auto& column = matrix.row(r);
    auto max_val = column.max(col);
    const arma::uvec maxs = arma::find(column == max_val);
    std::uniform_int_distribution<int> dis(0, maxs.size() - 1);
    result(r) = maxs[dis(rng)];
  }
  return result;
}

/* Computes the column index with the maximum value, for each row. */
template <typename RandomGenerator = std::mt19937>
arma::rowvec max_col(const arma::mat& matrix, TieMethod tie = TieMethod::Random,
                     RandomGenerator rng = ::core::global_random) {
  switch (tie) {
    case TieMethod::First:
      return matrix_max_col_first(matrix);
    case TieMethod::Last:
      return matrix_max_col_last(matrix);
    case TieMethod::Random:
      return matrix_max_col_random(matrix, rng);
  }
  throw std::runtime_error("Invalid tie-method resolution");
}

static inline arma::uword max_rowvec_first(const arma::rowvec& row) {
  arma::uword col;
  row.max(col);
  return col;
}

static inline arma::uword max_rowvec_last(const arma::rowvec& row) {
  arma::uword col;
  const auto max_val = row.max(col);
  const arma::uvec values = arma::find(row == max_val);
  return values[values.size() - 1];
}

template <typename RandomGenerator>
arma::uword max_rowvec_random(const arma::rowvec& row, RandomGenerator rng) {
  arma::uword col;
  auto max_val = row.max(col);
  const arma::uvec values = arma::find(row == max_val);
  std::uniform_int_distribution<int> dis(0, values.size() - 1);
  return values[dis(rng)];
}

/* Computes the column index with the maximum value, for each row. */
template <typename RandomGenerator = std::mt19937>
arma::uword max_rowvec(const arma::rowvec& row,
                       TieMethod tie = TieMethod::Random,
                       RandomGenerator rng = ::core::global_random) {
  switch (tie) {
    case TieMethod::First:
      return max_rowvec_first(row);
    case TieMethod::Last:
      return max_rowvec_last(row);
    case TieMethod::Random:
      return max_rowvec_random(row, rng);
  }
  throw std::runtime_error("Invalid tie-method resolution");
}

}  // namespace arma

namespace core {

extern std::string expand_environmental_vars(std::string str);

}  // namespace core

#endif  // UTILITY_H_
