#ifndef CORE_COMBINATIONS_H_
#define CORE_COMBINATIONS_H_

#include <cstddef>

#include <functional>
#include <iterator>
#include <stdexcept>
#include <vector>

namespace core {

/* Generate all combinations of N containers.
 * Example:
 * vector<vector<string>> vectors = {{{"a", "b", "c"}, {"1", "2"}}};
 * Combinations<decltype(vectors)> comb = vectors;
 * Now you can iterate over |comb|. Each combination is a vector of iterators,
 * one iterator for each of N containers. Iterations will generate:
 * a 1
 * a 2
 * b 1
 * b 2
 * c 1
 * c 2
 * Note that Combinations<> only store a reference to the input.
 * */
template <typename ListListType>
class Combinations {
 public:
  using list_t = typename ListListType::value_type;
  using const_iterator_vector_t = std::vector<typename list_t::const_iterator>;

  Combinations(const ListListType& lists) : lists_(lists) {
    if (lists_.empty()) std::runtime_error("Combination lists cannot be empty");
  }

  class const_iterator : public std::iterator<std::forward_iterator_tag,
                                              const_iterator_vector_t> {
   public:
    const_iterator(const ListListType& lists, bool sentinel) : lists_(lists) {
      iterators_.resize(lists.size());
      reset(sentinel);
    }

    const_iterator(const const_iterator& other)
        : lists_(other.lists_), iterators_(other.iterators_){};

    const_iterator& operator++() {
      next();
      return *this;
    }

    const_iterator& operator++(int) = delete;

    bool operator==(const const_iterator& other) {
      if (iterators_.size() != other->size()) return false;
      for (std::size_t i = 0; i < iterators_.size(); ++i) {
        if (iterators_.at(i) != other->at(i)) return false;
      }
      return true;
    }

    bool operator!=(const const_iterator& other) { return !(*this == other); }

    const const_iterator_vector_t* operator->() const { return &iterators_; }

    const const_iterator_vector_t& operator*() const { return iterators_; }

   private:
    bool end() const { return iterators_[0] == lists_[0].end(); }

    void reset(bool sentinel) {
      std::size_t i, N = lists_.size();
      if (sentinel)
        for (i = 0; i < N; ++i) iterators_[i] = lists_[i].end();
      else
        for (i = 0; i < N; ++i) iterators_[i] = lists_[i].begin();
    }

    void next() {
      std::size_t i, N = lists_.size();
      ++iterators_[N - 1];
      for (i = N - 1; i > 0 && iterators_[i] == lists_[i].end(); --i) {
        iterators_[i] = lists_[i].begin();
        ++iterators_[i - 1];
      }
      if (end()) reset(true);
    }

    const ListListType& lists_;
    const_iterator_vector_t iterators_;
  };

  const_iterator begin() { return const_iterator(lists_, false); }
  const_iterator end() { return const_iterator(lists_, true); }

  // TODO: remove
  std::size_t dim() const { return lists_.size(); }

 private:
  const ListListType& lists_;
};

}  // namespace core

#endif  // CORE_COMBINATIONS_H_
