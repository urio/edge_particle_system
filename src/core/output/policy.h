#ifndef CORE_OUTPUT_POLICY_H_
#define CORE_OUTPUT_POLICY_H_

#include <ostream>
#include <memory>
#include <string>
#include <utility>

#include <boost/property_tree/ptree.hpp>

namespace core {

/* Manages the output of many algorithms running in threads.
 * This class provides thread-safe functions (synced_ + operation) that should
 * not be overridden. However, synced_ + operation functions call virtual
 * functions "operation" that can define their own behavior accordingly. */
class OutputPolicy {
 public:
  using Tree = boost::property_tree::ptree;
  using OutputType = std::pair<std::string, std::unique_ptr<std::ostream>>;

  OutputPolicy(const Tree& prop);
  virtual ~OutputPolicy();

  /* Get one received item. */
  virtual OutputType pop();

  /* Add one received item. */
  virtual void push(OutputType item) = 0;

  /* Check whether |identifier| has been processed already. */
  virtual bool contains(const std::string& identifier);

  /* Asks to flush everything that is still being held in memory, or waiting to
   * be processed. */
  virtual void flush();

  /* An output policy may provide a prefix for each identifier. */
  virtual const std::string& prefix() const;

  /* Get the correct OutputPolicy instance according to |pt| specification.
   * |pt| usually is rooted at node "output": {} of a specification. */
  static std::unique_ptr<OutputPolicy> make(const Tree& pt);

 private:
 const std::string prefix_;
};

}  // namespace core

#endif  // CORE_OUTPUT_POLICY_H_
