#include "./policy.h"

#include <iostream>
#include <stdexcept>
#include <memory>

#include "core/debug.h"
#include "core/filesystem.h"

#include "./file.h"
#include "./none.h"
#include "./terminal.h"
#ifdef WITH_REDIS
#include "./redis.h"
#endif

#include "core/utility.h"

namespace core {

OutputPolicy::OutputPolicy(const Tree& prop)
    : prefix_(
          filesystem::ensure_trailing_separator(core::expand_environmental_vars(
              prop.get<std::string>("prefix", "./")))) {}

OutputPolicy::~OutputPolicy() {}

OutputPolicy::OutputType OutputPolicy::pop() {
  throw std::runtime_error("pop operation not supported");
}

bool OutputPolicy::contains(const std::string& identifier) {
  return filesystem::exists(identifier);
}

void OutputPolicy::flush() {}

std::unique_ptr<OutputPolicy> OutputPolicy::make(const Tree& pt) {
  const std::string type = pt.get<std::string>("type");
  if (type == "terminal") return std::make_unique<TerminalOutput>(pt);
  if (type == "file") return std::make_unique<FileOutput>(pt);
  if (type == "none") return std::make_unique<NoneOutput>(pt);

  if (type == "redis") {
#ifdef WITH_REDIS
    return std::make_unique<RedisOutput>();
#else
    printd(ERROR, "Output policy '%s' was disabled at compile time.\n",
           type.c_str());
    throw std::runtime_error("Invalid output policy");
#endif
  }  // redis
  printd(ERROR, "'%s' is not a valid output policy.\n", type.c_str());
  throw std::runtime_error("Invalid output policy");
}

const std::string& OutputPolicy::prefix() const { return prefix_; }

}  // namespace core
