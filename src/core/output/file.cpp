#include "./file.h"

#include "../debug.h"
#include "../filesystem.h"

#include <fstream>

namespace core {

FileOutput::FileOutput(const Tree& prop)
    : OutputPolicy(prop) {
  if (!prefix().empty() && !filesystem::exists(prefix()) &&
      !filesystem::ensure_path(prefix())) {
    printd(ERROR, "Cannot create output path '%s'.\n", prefix().c_str());
    throw std::runtime_error("Cannot create output path");
  }
}

FileOutput::~FileOutput() {}

void FileOutput::push(OutputType item) {
  try {
    std::ofstream file;
    file.open(item.first);
    file << item.second.get()->rdbuf();
    file.close();
  } catch (...) {
    printd(ERROR, "Cannot write to file %s.\n", item.first.c_str());
  }
}

bool FileOutput::contains(const std::string& identifier) {
  return filesystem::exists(identifier);
}

}  // namespace core
