#ifndef CORE_OUTPUT_POLICY_NONE_H_
#define CORE_OUTPUT_POLICY_NONE_H_

#include <ostream>

#include "./policy.h"

namespace core {

class NoneOutput : public OutputPolicy {
 public:
  NoneOutput(const Tree&);
  virtual ~NoneOutput();

  virtual void push(OutputType);
};

}  // namespace core

#endif  // CORE_OUTPUT_POLICY_NONE_H_
