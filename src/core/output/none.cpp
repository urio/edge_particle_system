#include "./none.h"

#include "../filesystem.h"

#include "core/debug.h"

namespace core {

NoneOutput::NoneOutput(const Tree& prop) : OutputPolicy(prop) {}

NoneOutput::~NoneOutput() {}

void NoneOutput::push(OutputType) {}

}  // namespace core
