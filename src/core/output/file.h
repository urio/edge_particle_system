#ifndef CORE_OUTPUT_POLICY_FILE_H_
#define CORE_OUTPUT_POLICY_FILE_H_

#include <ostream>

#include "./policy.h"

namespace core {

class FileOutput : public OutputPolicy {
 public:
  FileOutput(const Tree& prop);
  virtual ~FileOutput();

  virtual void push(OutputType item);
  virtual bool contains(const std::string& identifier);
};

}  // namespace core

#endif  // CORE_OUTPUT_POLICY_FILE_H_
