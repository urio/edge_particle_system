#ifndef CORE_OUTPUT_POLICY_REDIS_H_
#define CORE_OUTPUT_POLICY_REDIS_H_

#include "./policy.h"

#include "cpp_redis/cpp_redis"

namespace core {

class RedisOutput : public OutputPolicy {
 public:
  RedisOutput(const Tree&);
  virtual ~RedisOutput();

  virtual void push(OutputType item);
  virtual bool contains(const std::string&);

 private:
  cpp_redis::redis_client client_;
  bool should_exit = false;
  cpp_redis::redis_client client;
};

}  // namespace core

#endif  // CORE_OUTPUT_POLICY_REDIS_H_
