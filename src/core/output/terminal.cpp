#include "./terminal.h"

#include <iostream>

namespace core {

TerminalOutput::TerminalOutput(const Tree& prop)
    : OutputPolicy(prop), output_(get_stream(prop)) {}

TerminalOutput::TerminalOutput(std::ostream& output)
    : OutputPolicy(Tree()), output_(output) {}

TerminalOutput::~TerminalOutput() {}

std::ostream& TerminalOutput::get_stream(const Tree& prop) {
  std::string name = prop.get<std::string>("stream", "stderr");
  if (name == "stderr")
    return std::cerr;
  else if (name == "stdout")
    return std::cout;
  throw std::runtime_error("Invalid stream");
}

void TerminalOutput::push(OutputType item) {
  std::lock_guard<std::mutex> guard(mutex_);
  output_ << "Identifier: " << item.first << "\n";
  output_ << item.second.get()->rdbuf();
}

void TerminalOutput::flush() { output_.flush(); }

}  // namespace core
