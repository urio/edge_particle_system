#ifndef CORE_OUTPUT_POLICY_TERMINAL_H_
#define CORE_OUTPUT_POLICY_TERMINAL_H_

#include <ostream>
#include <mutex>

#include "./policy.h"

namespace core {

class TerminalOutput : public OutputPolicy {
 public:
  TerminalOutput(const Tree& prop);
  TerminalOutput(std::ostream& output);
  virtual ~TerminalOutput();

  virtual void push(OutputType item);
  virtual void flush();

 private:
  std::ostream& get_stream(const Tree& prop);

  std::ostream& output_;
  std::mutex mutex_;
};

}  // namespace core

#endif  // ifndef CORE_OUTPUT_POLICY_TERMINAL_H_
