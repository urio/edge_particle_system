#include "./redis.h"

#include "../debug.h"

#include <unistd.h>

#include <condition_variable>
#include <sstream>

namespace core {

RedisOutput::RedisOutput(const Tree& prop) : OutputPolicy(prop) {
  client_.connect();
  client_.set_disconnection_handler(
      [&](cpp_redis::redis_client&) { should_exit = true; });
}

RedisOutput::~RedisOutput() {
  printd(TESTING, "Disconnecting from Redis\n");
  client_.disconnect();
  while (!should_exit) usleep(100);
}

void RedisOutput::push(OutputType item) {
  std::stringstream ss;
  ss << item.second.get()->rdbuf();
  client_.send({"SET", item.first, ss.str()});
  printd(TESTING, "SET %s: %s\n", item.first.c_str(), ss.str().c_str());
}

bool RedisOutput::contains(const std::string& identifier) {
  bool returned = false, result;
  client_.send({"EXISTS", identifier}, [&](const auto& reply) {
    result = reply.as_integer();
    returned = true;
  });
  while (!returned) usleep(100);
  return result;
}

}  // namespace core
