#ifndef EDGE_LISTS_H_
#define EDGE_LISTS_H_

#include <cstddef>

#include <vector>

#include <igraphpp/igraph.hpp>

namespace igraph {

// TODO Make access to vector as readonly, to keep consistency
class EdgeLists {
 public:
  EdgeLists();
  EdgeLists(const EdgeLists&) = delete;
  EdgeLists(EdgeLists&&) = delete;
  EdgeLists(const Graph& graph);
  ~EdgeLists();

  void BuildLists(const Graph& graph);

  typedef std::vector<std::vector<std::size_t>>::const_iterator
    const_iterator;
  typedef std::vector<std::vector<std::size_t>>::size_type
    size_type;

  const std::vector<std::size_t>& at(size_type i) const {
    return nei_.at(i);
  }

  const std::vector<std::size_t>& operator[](size_type i) const {
    return nei_.at(i);
  }

  const std::vector<std::vector<int>>& eids() const { return eids_; }

  const_iterator begin() const { return nei_.cbegin(); }
  const_iterator end() const { return nei_.cend(); }

  size_type size() const { return nei_.size(); }

 private:
  std::vector<std::vector<int>> eids_;
  std::vector<std::vector<std::size_t>> nei_;
};

}  // namespace core

#endif  // EDGE_LISTS_H_
