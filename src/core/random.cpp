#include "./random.h"

#include <random>

namespace core {

static std::random_device rnd_device;

std::mt19937 global_random(rnd_device());



} // namespace core

