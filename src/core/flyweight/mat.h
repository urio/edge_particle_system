#ifndef CORE_FLYWEIGHT_MAT_H_
#define CORE_FLYWEIGHT_MAT_H_

#include <armadillo>

#include <boost/flyweight.hpp>
#include <boost/flyweight/key_value.hpp>
#include <boost/flyweight/no_tracking.hpp>
#include <boost/flyweight/simple_locking.hpp>
#include <igraphpp/igraph.hpp>

#include <core/debug.h>
#include <core/file_information.h>
#include <core/edge_lists.h>
#include <core/edge_vector.h>

namespace core {

namespace flyweight {

class mat_t {
 public:
  using File = ::core::FileInformation;

  mat_t(const File& file) : file_(file) {
    mat_.load(file.name, arma::raw_ascii);
    printd(INFO, "Loaded file %s\n", file_.name.c_str());
  }

  mat_t(const mat_t& network) = delete;  //: file_(network.file_) {}

  ~mat_t() { printd(INFO, "Releasing file %s\n", file_.name.c_str()); }

  const File& file() const { return file_; }

  arma::mat const* mat() const { return &mat_; }

 private:
  const File file_;
  arma::mat mat_;
};

struct mat_fileinfo_extractor {
  const ::core::FileInformation& operator()(const mat_t& network) const {
    return network.file();
  }
};

struct mat_tag {};

typedef boost::flyweight<
    boost::flyweights::key_value<core::FileInformation, mat_t,
                                 mat_fileinfo_extractor>,
    boost::flyweights::tag<mat_tag>, boost::flyweights::no_tracking,
    boost::flyweights::simple_locking> Mat;

}  // namespace flyweight

}  // namespace core

#endif  // CORE_FLYWEIGHT_MAT_H_
