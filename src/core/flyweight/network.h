#ifndef CORE_FLYWEIGHT_NETWORK_H_
#define CORE_FLYWEIGHT_NETWORK_H_

#include <boost/flyweight.hpp>
#include <boost/flyweight/key_value.hpp>
#include <boost/flyweight/no_tracking.hpp>
#include <boost/flyweight/simple_locking.hpp>
#include <igraphpp/igraph.hpp>

#include <core/debug.h>
#include <core/file_information.h>
#include <core/edge_lists.h>
#include <core/edge_vector.h>

namespace core {

namespace flyweight {

class network_t {
 public:
  using File = ::core::FileInformation;

  network_t(const File& file)
      : file_(file),
        graph_(igraph::Graph::ReadPajek(file_.name).to_undirected()),
        edge_vector_(graph_),
        edge_lists_(graph_) {
    printd(INFO, "Loaded network %s\n", file_.name.c_str());
  }

  network_t(const network_t& network) = delete;  //: file_(network.file_) {}

  ~network_t() { printd(INFO, "Releasing network %s\n", file_.name.c_str()); }

  const File& file() const { return file_; }

  igraph::Graph const* graph() const { return &graph_; }
  igraph::EdgeVector const* edge_vector() const { return &edge_vector_; }
  igraph::EdgeLists const* edge_lists() const { return &edge_lists_; }

 private:
  const File file_;
  igraph::Graph graph_;
  igraph::EdgeVector edge_vector_;
  igraph::EdgeLists edge_lists_;
};

struct network_fileinfo_extractor {
  const ::core::FileInformation& operator()(const network_t& network) const {
    return network.file();
  }
};

struct network_tag {};

typedef boost::flyweight<
    boost::flyweights::key_value<core::FileInformation, network_t,
                                 network_fileinfo_extractor>,
    boost::flyweights::tag<network_tag>, boost::flyweights::refcounted,
    boost::flyweights::simple_locking> Network;

}  // namespace flyweight

}  // namespace core

#endif  // CORE_FLYWEIGHT_NETWORK_H_
