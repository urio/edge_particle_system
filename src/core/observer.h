#ifndef OBSERVER_H_
#define OBSERVER_H_

namespace core {

template <class Subject>
class Observer {
 public:
  Observer(Subject* subject) : subject_(subject) { subject->attach(this); }

  virtual void update() = 0;

  const Subject* subject() const { return subject_; }
  Subject* subject() { return subject_; }

 private:
  Subject* subject_;
};

}  // namespace core

#endif  // OBSERVER_H_
