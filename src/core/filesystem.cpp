#include "./filesystem.h"

#include <sys/stat.h>

#include <cstdio>

#include <string>

namespace filesystem {

constexpr char kPathSeparator = '/';

static const char kDirectorySeparator = '/';

std::string extract_path(const std::string &filename) {
  const size_t last_slash_idx = filename.rfind(kDirectorySeparator);
  std::string directory;

  if (std::string::npos != last_slash_idx) {
    directory = filename.substr(0, last_slash_idx);
  }
  return directory;
}

void ensure_trailing_separator(std::string &&path) {
  if (!path.empty() && path.back() != kPathSeparator) {
    path.append(1, kPathSeparator);
  }
}
std::string ensure_trailing_separator(const std::string &path) {
  if (path.empty() || path.back() == kPathSeparator) return path;

  return std::string(path + kPathSeparator);
}

bool exists(const std::string &filename) {
  struct stat buffer;

  return (stat(filename.c_str(), &buffer) == 0);
}

bool remove(const std::string &filename) {
  int ret = std::remove(filename.c_str());
  return ret == 0;
}

bool ensure_path(const std::string &filename) {
  std::string directory = extract_path(filename);
  int ret = 1;

  if (directory.size() > 0 && !exists(directory)) {
    ret = mkdir(directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }
  return ret == 0;
}

}  // namespace filesystem
