#include "./edge_lists.h"

namespace igraph {

EdgeLists::EdgeLists() : eids_() {}
EdgeLists::EdgeLists(const Graph& graph) : eids_() { BuildLists(graph); }
EdgeLists::~EdgeLists() {}

void EdgeLists::BuildLists(const Graph& graph) {
  std::size_t V = static_cast<std::size_t>(graph.vcount());
  Vector degrees = graph.degree();

  nei_.clear();
  eids_.clear();
  nei_.resize(V);
  eids_.resize(V);
  for (std::size_t v = 0; v < V; ++v) {
    auto& adjlist = nei_.at(v);
    auto& eidlist = eids_.at(v);
    Vector neighbors = graph.neighbors(static_cast<int>(v));

    adjlist.reserve(degrees[v]);
    eidlist.reserve(degrees[v]);
    for (double nei : neighbors) {
      adjlist.push_back(static_cast<std::size_t>(nei));
      eidlist.push_back(graph.eid(static_cast<int>(v), static_cast<int>(nei)));
    }
  }
}

}  // namepace igraph
