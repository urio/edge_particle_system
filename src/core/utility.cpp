#include "./utility.h"

#include <cstdlib>

#include <list>
#include <string>
#include <utility>

#include <iostream>

#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace core {

std::string expand_environmental_vars(std::string str) {
  const boost::regex envscan(R"(\$\{([0-9A-Za-z_\/]*)\})");
  const boost::sregex_iterator end;
  using str_list_t =
      std::list<std::tuple<const std::string, const std::string>>;

  str_list_t replacements;
  for (boost::sregex_iterator rit(str.begin(), str.end(), envscan); rit != end;
       ++rit) {
    replacements.push_back(std::make_pair((*rit)[0], (*rit)[1]));
  }

  for (const auto& lit : replacements) {
    const char* expanded = std::getenv(std::get<1>(lit).c_str());
    if (expanded != NULL) {
      boost::replace_all(str, std::get<0>(lit), expanded);
    }
  }

  return str;
}

}  // namespace core
