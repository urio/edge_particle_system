#include "./specification.h"

#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>

#include <boost/algorithm/string/replace.hpp>
#include <boost/regex.hpp>

#include "./combinations.h"
#include "./debug.h"

namespace core {

Specification::Specification() {}
Specification::~Specification() {}

void Specification::read(std::istream& json) {
  boost::property_tree::read_json(json, pt);
  ProcessTree();
}

std::string Specification::problem() const {
  return pt.get<std::string>("problem");
}
std::string Specification::algorithm() const {
  return pt.get<std::string>("algorithm.name");
}

OutputPolicy& Specification::output() {
  if (!output_) output_ = OutputPolicy::make(Node("output"));
  return *output_.get();
}

Specification::Tree Specification::Node(const std::string& node_path) const {
  return pt.get_child(node_path);
}

}  // namespace core
